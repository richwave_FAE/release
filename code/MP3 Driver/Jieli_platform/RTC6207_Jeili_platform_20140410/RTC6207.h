/********************************************************
RTC62XX driver function claim
********************************************************/
#ifndef RTC6207_DRV_H
#define RTC6207_DRV_H

#include "typedef.h"


bool RTC6207_Read_ID(void);
void init_RTC6207(void);
void RTC6207_Power_Up(void);
void RTC6207_powerdown(void);
void RTC6207_mute(u8 Enable_Mute);
void rtc6207_setch(u8 dat);
bool set_freq_RTC6207(u16 freq);
u8 RTC6207_FM_SW_Seek(u16 channel_freq, u8 channel_space);


typedef enum OPERA_MODE {
    READ = 1,
    WRITE = 2
} T_OPERA_MODE;


typedef enum SEEK_MODE {
    SEEKDOWN_HALT = 1,
    SEEKDOWN_WRAP = 2,
    SEEKUP_HALT = 3,
    SEEKUP_WRAP = 4
} T_SEEK_MODE;

#endif












