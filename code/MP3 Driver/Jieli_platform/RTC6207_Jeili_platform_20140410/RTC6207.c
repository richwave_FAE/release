

/*******************************************************************************

 *******************************************************************************/

#include "FM_API.h"

#ifdef	RTC6207

#include "rtc6207.h"
#include "iic.h"

#define FM_TUNER_GRID         10

const u8  RTC62XX_power_up[] AT(RTC6207_TABLE_CODE)=
{
	//space 50kHz
	//0xC0,0x01,0x00,0x00,0x90,0x04,0x0c,0x2f,0x00,0x48,0x81,0x00,0x00,0x00,  
	//0x40,0x50
	
    0xC0,0x01,0x00,0x00,0x90,0x04,0x0c,0x1f,0x00,0x48,0x81,0x00,0x00,0x00, 
    0x40,0x50
};  // last byte is spike threshold, the larger the more, recommand under 0x50, default 0x10

const u8  RTC62XX_Initial_value[] AT(RTC6207_TABLE_CODE)=
{
    0xC0,0x01,0x00,0x00,0x90,0x04,0x0c,0x1f,0x00,0x48,0x80,0x00,0xe0,0x00
};	// RSSI_TH = 0x0c


const u8  RTC62XX_Initial_value1[] AT(RTC6207_TABLE_CODE) =
{
	0x00,0x72,0x00,0xff,0x00,0x00,0x03,0xff,0x11,0x00,0x00,0x00,0x00,0x00
};

const u8  RTC62XX_CKHopping[] AT(RTC6207_TABLE_CODE)=
{
    0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x20,0x00,
    0x05,0x0F, 0x40,0x00, 0x10,0x40, 0x00,0x11, 0x00,0x00, 0x00,0x00, 0x00,0x00,
    0xD0,0x09, 0x7F,0x80, 0x3C,0x01, 0x96,0xC0, 0x01,0x00, 0x00,0x00, 0x01,0x40,
    0x47,0x00, 0x00,0x00
};		


//	25ms+55ms=80*205 + sw_p_time ~ 19s
const u8  RTC62XX_SWBank2[] AT(RTC6207_TABLE_CODE)=
{
    0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x80,0x00, 0x80,0x00,
    0x01,0x01, 0xC8,0xC8, 0x07,0x2D, 0x05,0x1C, 0x03,0x0B, 0x03,0x0B, 0x07,0x2D,
    0x00,0x83, 0x7F,0x8A, 0xC8,0x01, 0xC8,0xC8, 0x14,0x00, 0x40,0x00, 0x00,0x01,
    0x32,0x01, 0x00,0x00
};


const u8  RTC62XX_power_down[] AT(RTC6207_TABLE_CODE)=
{
    0x00,0x41,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};


void rtc6207_read(u8 *dat,u8 n)AT(RTC6207_CODE)
{
    // iic_readn(0x21,0xff,dat,n);
    app_IIC_readn(0x21,0xff,dat,n);
}

void rtc6207_write(u8 *dat,u8 n)AT(RTC6207_CODE)
{
    //iic_write(0x20,0xff,dat,n);
    app_IIC_write(0x20,0xff,dat,n);
}


/**************************************
RTC6207_FM_Tune_Freq()

channel_freq: 8750~10800
channel_space: 50,100,200
***************************************/
u8 RTC6207_FM_SW_Seek(u16 channel_freq, u8 channel_space)AT(RTC6207_CODE)
{
    u16  freq_reg_data, loop_counter;
    u8  RTC6207_reg_data[24];
    u8  RTC6207_channel_start_tune[] = {0xC0,0x01,0x80,0xCA};	//107.7MHz
    u8  RTC6207_channel_stop_tune[] = {0xC0,0x01,0x00,0xCA};	//changtt

    u8  RSSIValue = 0;

    //my_printf("channel_freq=%d\n", channel_freq);
    freq_reg_data = (channel_freq - 8750)/(channel_space/10);
    RTC6207_channel_start_tune[3] = freq_reg_data & 0xff;
    RTC6207_channel_start_tune[2] = (RTC6207_channel_start_tune[2] & 0xfc) | (freq_reg_data >> 8);
    RTC6207_channel_stop_tune[3] = RTC6207_channel_start_tune[3] ;


    rtc6207_write(&(RTC6207_channel_start_tune[0]), 4);
    //wait STC=1
    do
    {
        rtc6207_read(&(RTC6207_reg_data[0]), 2);
        loop_counter++;
    }
    while(((RTC6207_reg_data[0]&0x40) == 0) && (loop_counter < 0xffff));		//for loop_counter, when tune, the loop time must > 60ms

    if(loop_counter == 0xffff)
        return 0;

    loop_counter = 0;

    //clear tune bit
    rtc6207_write(&(RTC6207_channel_stop_tune[0]), 4);
    //wait STC=0
    do
    {
        rtc6207_read(&(RTC6207_reg_data[0]), 2);
        loop_counter++;
    }
    while(((RTC6207_reg_data[0]&0x40) != 0) && (loop_counter < 0xff));

    rtc6207_write((u8 *)&(RTC62XX_Initial_value[0]), 14);
    rtc6207_read(&(RTC6207_reg_data[0]), 24);


    rtc6207_write((u8 *)&RTC62XX_Initial_value1[0], 14);
    // delay_n10ms(1);

    if((RTC6207_reg_data[22]&0x80)!= 0)
    {
        return 0;
    }

    rtc6207_read(&(RTC6207_reg_data[0]), 2);

    RSSIValue = RTC6207_reg_data[1];


    if(RSSIValue > RTC62XX_Initial_value[6])
        return 1;
    else
        return 0;

}


/*
u8 RTC6207_FM_Seek_Start(u8 seek_mode,u8 * pChannel_Freq)AT(RTC6207_CODE)
{
	u16  freq_reg_data, loop_counter = 0;
	u8  RTC6207_channel_seek_start[] = {0xC1,0x01};
	u8  RTC6207_channel_seek_stop[] = {0xC0,0x01};
	u8  RTC6207_reg_data[32];
	u8  channelpass = 0;
	u8  channelpass1 = 0;
       u8 channel_space = 100;

	switch(seek_mode)
	{
		case SEEKDOWN_HALT:
		{
			RTC6207_channel_seek_start[0] = 0xC5;
		}
		case SEEKDOWN_WRAP:
		{
			RTC6207_channel_seek_start[0] = 0xC1;
			break;
		}
		case SEEKUP_HALT:
		{
			RTC6207_channel_seek_start[0] = 0xC7;
			break;
		}
		case SEEKUP_WRAP:
		{
			RTC6207_channel_seek_start[0] = 0xC3;
			break;
		}
	}

	//freq_reg_data = (channel_freq - 8750)/(channel_space/10);

	//set seek bit
 	//error_ind = OperationRTC62XX_2w(WRITE,&(RTC6207_channel_seek_start[0]), 2);

    rtc6207_write(&(RTC6207_channel_seek_start[0]), 2);

	//error_ind = OperationRTC62XX_2w(WRITE,&(RTC6207_channel_seek_start[0]), 10);
//	if(error_ind)
//		return I2C_ERROR;

	RTC6207_reg_data[3] = 0;
	
	//wait STC=1
	do
	{
		channelpass = RTC6207_reg_data[3];
		//error_ind = OperationRTC62XX_2w(READ,&(RTC6207_reg_data[0]), 2);
		//error_ind = OperationRTC62XX_2w(READ,&(RTC6207_reg_data[0]), 4); //changtt//4

        rtc6207_read(&(RTC6207_reg_data[0]),4);

		//if(error_ind)
		//	return I2C_ERROR;
		channelpass1 = RTC6207_reg_data[3];

        rtc6207_read(&(RTC6207_reg_data[0]),4);
	//	error_ind = OperationRTC62XX_2w(READ,&(RTC6207_reg_data[0]), 4); //changtt software BL
	//	if(error_ind)
	//		return I2C_ERROR;

		if(channelpass1 != RTC6207_reg_data[3])
		{
		//	error_ind = OperationRTC62XX_2w(READ,&(RTC6207_reg_data[0]), 4); //changtt software BL
		//	if(error_ind)
		//		return I2C_ERROR;
			//dbg_print("CP0 = %d, CP1 = %d, CP2 = %d \n\r", channelpass, channelpass1, RTC6207_reg_data[3]);
		    rtc6207_read(&(RTC6207_reg_data[0]),4);
        }
		loop_counter++;
	}
	while(((RTC6207_reg_data[0]&0x40) == 0) && (loop_counter < 0xffff) && (RTC6207_reg_data[3] >= channelpass));	//for loop_counter, when seek, the loop time must > 12s	at the worst case
																																			//60ms*((108-87.5)/0.1+1)= 12s

//	dbg_print("RegData[0] = %x, Smode = %d, ErrInd = %d \n\r", RTC6207_reg_data[0], seek_mode, error_ind);

    //printf_u16(loop_counter);
	if(loop_counter == 0xffff)
    	return 0;
    putchar('B');
	if(RTC6207_reg_data[3] < channelpass)		// software BL = 1, disable seek and return software BL =1
	{
	//	error_ind = OperationRTC62XX_2w(WRITE,&(RTC6207_channel_seek_stop[0]), 2);
	//	if(error_ind)
	//		return I2C_ERROR;
        rtc6207_write(&(RTC6207_channel_seek_stop[0]),2);
		//SeekFail = 1;
		//dbg_print("FMSeekS0-3 = %x, %d \n\r", loop_counter, RTC6207_reg_data[3]);
		return 0;
	}
	//dbg_print("FMSeekS1 = %d, %d \n\r", error_ind, RTC6207_reg_data[3]);
    putchar('C');

	loop_counter = 0;



	//you can check AFC Rail here  //Richwave Chip must avoid this setting due to no AFC Rail bit
	//if((RTC6207_reg_data[0]& 0x10) != 0)
		//valid_channel = 0;


	//Read REG0A&0B for channel number or RSSI here
//	error_ind = OperationRTC62XX_2w(READ,&(RTC6207_reg_data[0]), 4);
//	if(error_ind)
//		return I2C_ERROR;
//	dbg_print("FMSeekS2-2 = %d, %d \n\r", error_ind, RTC6207_reg_data[3]);
    rtc6207_read(&(RTC6207_reg_data[0]),4);

	freq_reg_data = ((u16)((u16)RTC6207_reg_data[2] << 8) | RTC6207_reg_data[3])& 0x3ff;
//	dbg_print("FMSeekS2-3 = %d, %d \n\r", channel_space, freq_reg_data);

	*pChannel_Freq = 8750 + freq_reg_data*(channel_space/10);
     //printf_u16((u16)*pChannel_Freq );
//	dbg_print("FMSeekS2 = %x, %d \n\r", loop_counter, RTC6207_reg_data[3]);


//	#ifndef FM_50KHZ_STEP //changtt
//	//	*pChannel_Freq /= 10; //changtt
//	#endif

//	dbg_print("FMSeekS3-1 = %d, %d \n\r", loop_counter, RTC6207_reg_data[3]);

	 rtc6207_write(&(RTC6207_channel_seek_stop[0]),2);
	//clear seek bit
//	error_ind = OperationRTC62XX_2w(WRITE,&(RTC6207_channel_seek_stop[0]), 2);
	//error_ind = OperationRTC62XX_2w(WRITE,&(RTC6207_channel_seek_stop[0]), 10);
//	if(error_ind)
	//	return I2C_ERROR;

     putchar('D');
	//wait STC=0
	do
	{
        rtc6207_read(&(RTC6207_reg_data[0]),2);
	//	error_ind = OperationRTC62XX_2w(READ,&(RTC6207_reg_data[0]), 2);
	//	if(error_ind)
	//		return I2C_ERROR;
		loop_counter++;
	}
	while(((RTC6207_reg_data[0]&0x40) != 0) && (loop_counter < 0xff));
//	dbg_print("FMSeekS3 = %x, %d \n\r", loop_counter, RTC6207_reg_data[1]);

	if(loop_counter >= 0xff)
        return 0;

   // deg_string("f  ok  \n");
	return 1;
}
*/

/**********************************************************************
*  Radio valid station,used in auto search frequency to verify
*  valid positon
*
*  parameter-->freq: frequency
               signalv1:signal level range is 0 ~ 15
               is_step_up:return value(Reserved)
**********************************************************************/
__root bool set_freq_RTC6207(u16 freq)AT(RTC6207_CODE)   //设置频点  和  判断频点是否为真
{
    u16 curFreq = (u16)freq * FM_TUNER_GRID;
    //my_printf("Current freq = %d\n",freq );

    if(RTC6207_FM_SW_Seek(curFreq,100) !=0)
        return 1;
    else
        return 0;
}

void RTC6207_Power_Up(void)AT(RTC6207_CODE)
{

    rtc6207_write((u8 *)&(RTC62XX_CKHopping[0]),46);
    delay_n10ms(1);
    //rtc6207_write((u8 *)&(RTC62XX_SWBank2[0]),46);
    //delay_n10ms(1);
    rtc6207_write((u8 *)&(RTC62XX_power_up[0]),16);
    delay_n10ms(5);
}

__root void RTC6207_powerdown(void)AT(RTC6207_CODE)
{
    //rtc6207_write((u8 *)&(RTC62XX_CKHopping1[0]), 46);
    delay_n10ms(1);
    rtc6207_write((u8 *)&(RTC62XX_power_down[0]), 12);
}


__root bool RTC6207_Read_ID(void)AT(RTC6207_CODE)   //读id
{
    u8 RTC62XX_reg_data[14];

    rtc6207_read(&(RTC62XX_reg_data[0]), 14);

    if(((RTC62XX_reg_data[12] << 8) | RTC62XX_reg_data[13])==0x1688)
    {
        RTC6207_Power_Up();
        return 1;
    }
    else
    {
        return 0;
    }
}


void init_RTC6207(void)AT(RTC6207_CODE)
{

}


__root void RTC6207_mute(u8 Enable_Mute)AT(RTC6207_CODE)
{
    u8  RTC62XX_set_property[] = {0xC0, 0x01};

    if(Enable_Mute)
        RTC62XX_set_property[0] = 0x00;
    else
        RTC62XX_set_property[0] = 0xC0;

    rtc6207_write(&(RTC62XX_set_property[0]), 2);
}


__root void rtc6207_setch(u8 dat)AT(RTC6207_CODE)
{
    dat = dat;
}
/**************************************

RTC6207_Set_Property_FM_Volume()

FM_Volumn: 0~15

***************************************/

void RTC6207_Set_Property_FM_Volume(unsigned char FM_Volumn)AT(RTC6207_CODE)
{
    //unsigned char RTC62XX_reg_data[32];

    if(FM_Volumn > 15) FM_Volumn = 15;
#ifndef FM_50KHZ_STEP
    unsigned char RTC62XX_set_property[] = {0xC0,0x01,0x00,0x00,0x90,0x04,0x0c,0x10};	//SNR threshold = 0x0006 = 6dB
#else
    unsigned char RTC62XX_set_property[] = {0xC0,0x01,0x00,0x00,0x90,0x04,0x0c,0x20};	//step 50K
#endif
    RTC62XX_set_property[7] = (RTC62XX_set_property[7] & 0xf0) | FM_Volumn;

    rtc6207_write(RTC62XX_set_property, 8);
}

#endif
