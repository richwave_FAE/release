/*
 ********************************************************************************
 213x -- i2c driver
 *                (c) Copyright 2007 - 2010, Actions Co,Ld.
 *                        All Right Reserved
 *
 * History:
 *      <author>      <time>       <version >    <desc>
 *       houjingkun    2008/06/18     1.0         build this file
 *       shaofeng      2010/04/20     1.1         加入调频回调函数
 *       shaofeng      2010/04/26     1.2         按照S227A架构整理代码, 将i2c做成通用库
 *       jamelee       2010/04/29     1.2         修正原i2c start与restart 的错误,
 *                                                调整相关代码以更独立化
 *       shaofeng      2010/05/12     1.4         增加根据设定的i2c频率, 动态计算超时值. 
 *       dengjihai     2010/11/30     1.5         增加向指定地址读/写数据函数，便于操作单个寄存器
 ********************************************************************************/
/*!
 * \file   i2c_comm.c
 * \brief
 *      i2c driver hardware layer source file.
 * \author houjingkun
 * \par GENERAL DESCRIPTION:
 *      provide some functions such as i2c_read and i2c_write and so on.
 * \par EXTERNALIZED FUNCTIONS:
 *
 *     Copyright(c) 2008-2012 Actions Semiconductor, All Rights Reserved.
 *
 * \version 1.0
 * \date  2008/06/18
 *******************************************************************************/
#include "include_fm.h"


#if CFG_GPIO_SIM_I2C == 0


/********************** I2C_CTL寄存器各bits ********************/
//transfer complete
#define I2C_STAT_TCB    (0x01<<7)
//stop detect bit
#define I2C_STAT_STPD   (0x01<<4)
//receive ack or nack
#define I2C_STAT_RACK   (0x01<<0)

/********************** I2C_CTL寄存器各bits ********************/
#define I2C_ENABLE           (0x01<<7)

#if CFG_I2C_INTERNAL_PULL_UP_RESISTOR_ENABLE == 1
#define I2C_PULLUP_ENABLE    (0x01<<6)
#else
#define I2C_PULLUP_ENABLE    (0x00)
#endif

#define I2C_GENERATE_START   (0x01<<2)
#define I2C_GENERATE_STOP    (0x02<<2)
#define I2C_GENERATE_RESTART (0x03<<2)

#define I2C_RELEASE_BUS      (0x01<<1)
#define I2C_GENERATE_NACK    (0x01<<0)



i2c_dev_t i2c_dev;

//在每次I2C init 的时候, 根据I2C速度, 计算出对应的超时时间
static unsigned int i2c_wait_tcb_time_out = 900;
static unsigned int i2c_wait_stop_time_out = 200;
//保存的i2c复用pin脚状态

//unsigned int saved_i2c;
static int (*p_request_clkadjust)(unsigned int clk, \
        void (*handler)(unsigned int clk, curclk_t *curclk, void *dev_id, unsigned int cmd, unsigned int direction), \
        void *dev_id);
static int (*p_free_clkadjust)(unsigned int clk, void *dev_id);
static int (*p_get_clock)(unsigned int cmd, unsigned int *result);

void i2c_get_gpio(int port)
{
#if 0    
    unsigned int i2c_gpio;
    unsigned int saved_gpio1;

    OS_INT_SAVE_AREA 
    OS_ENTER_CRITICAL();
    if (port == 0)
    {
        /* 保存串口的mfp设置 */
        saved_gpio1 = act_readl(MFP_CTL1);
        saved_i2c = (saved_gpio1 & 0x00006000) >> 13;

#if 0   /* 不需要，后面有 */       
        i2c_gpio = saved_gpio1 & 0xffff9fff; //multi-function: I2C1 SCL and SDA
        i2c_gpio = i2c_gpio | 0x80000000; //multi-function enable
        act_writel(i2c_gpio, MFP_CTL1);
#endif        
    }
    else
    {
        printk("\n\n【FM drviver】 i2c prot error..........\n");
        //;
    }
    OS_EXIT_CRITICAL();
    udelay(10);
#endif    
}

void i2c_release_gpio(int port)
{
#if 0     
    unsigned int saved_gpio1;
    
    OS_INT_SAVE_AREA 
    OS_ENTER_CRITICAL();
    if (port == 0)
    {
        /* 恢复串口的mfp设置 */
        saved_gpio1 = act_readl(MFP_CTL1);
        saved_gpio1 = saved_gpio1 & 0xffff9fff;
        saved_gpio1 = saved_gpio1 | (saved_i2c << 13);
        act_writel(saved_gpio1, MFP_CTL1);
    }
    else
    {
        printk("\n\n【FM drviver】 i2c prot error...........\n");
        //;
    }
    OS_EXIT_CRITICAL();
    udelay(10);
#endif    
}


/******************************************************************************/
/*!
* \par  Description:
*       i2c Transfer complete
* \param[in]    void
* \retval       int: -1:err; 0:succ
* \note
    wait to send or receive complete. if write, we need wait ack; if read don't.
*******************************************************************************/
static int _wait_tcb( void )
{
    unsigned int tmp = 0;
    unsigned int times = i2c_wait_tcb_time_out;

    do
    {
        --times;
        udelay(1);

        tmp = act_readl(i2c_dev.reg_I2C_STAT);
        tmp &= I2C_STAT_TCB;
    } while ( (tmp == 0) && (times > 0) );

    //clear tcb bit
    act_writel(tmp|I2C_STAT_TCB, i2c_dev.reg_I2C_STAT);

    if (times == 0)
    {
        printk_err("\n\ntime out.............");
        return -1;
    }
#if 0
//用于调试超时时间
    else
    {
        printk("\n\ntrans complete time:%d.............\n", times);
    }
#endif

    return 0;
}
/******************************************************************************/
/*!
* \par  Description:
*       wait ack to send complete
* \param[in]    void
* \retval       int: -1:nack; 0:succ ack
//ack
//        _
// SCL  _| |_
// SDA  _____

//nack
//        _
// SCL  _| |_
//      _____
// SDA
*******************************************************************************/
static int _wait_ack( void )
{
    unsigned int tmp = 0;

    //get ack or nack
    tmp = act_readl(i2c_dev.reg_I2C_STAT);

    if ( (tmp & I2C_STAT_RACK) > 0 )
    {
        return 0;
    }
    else
    {
        return -1;
    }
}


static unsigned int _apb_clk_get(void)
{
    unsigned int tmp;
    unsigned int corepll;
    unsigned int cclk_div;
    unsigned int sclk_div;
    unsigned int pclk_div;
    unsigned int pclk = 0;

    tmp = act_readl(CMU_COREPLL);
    tmp &= 0x7f;
    corepll = tmp * 6;
    cclk_div = ((act_readl(CMU_BUSCLK)>>4) & 0x3) + 1;
    sclk_div = ((act_readl(CMU_BUSCLK)>>6) & 0x3) + 1;
    pclk_div = ((act_readl(CMU_BUSCLK)>>8) & 0xF) + 1;
    if ( pclk_div < 2 )
    {
        //pclk_div最小值为2
        pclk_div = 2;
    }
    
    if ( (0!=cclk_div) && (0!=sclk_div) )
    {
        pclk = corepll / (cclk_div * sclk_div * pclk_div);
    }
    
    return pclk;
}

/******************************************************************************/
/*!
* \par  Description:
*       i2c 的调频函数
*       I2C_CLK*16=PCLK/I2C_CLKDIV
* \param[in]    unsigned int: apb clk, p clock. unit:M
* \retval       void
* \note
    i2c时钟源为pclk, 当系统调频后pclk变化, 则通过I2C_CLKDIV保持i2c速度不变
    I2C_CLKDIV的值必须>=2
*******************************************************************************/
void i2c_change_clk_div(unsigned int clk_div)
{
    //must be more than 1
    if( clk_div <=1 )
    {
        clk_div = 2;
    }
    
    act_writel(clk_div, i2c_dev.reg_I2C_CLKDIV);
}

/******************************************************************************/
/*!
* \par  Description:
*       i2c reset to default status
* \param[in]    null
* \retval       int: 0:succ, <0:err
* \note
*******************************************************************************/
void i2c_reset(void)
{
    act_writel(0x00000000, i2c_dev.reg_I2C_CTL); //reset I2C
    udelay(1);
    act_writel(I2C_ENABLE|I2C_PULLUP_ENABLE, i2c_dev.reg_I2C_CTL); //enable I2C
    udelay(1);
}


/******************************************************************************/
/*!
* \par  Description:
*       i2c start and transmit slave address
* \param[in]    unsigned int:i2c slave address
* \retval       int: -1:err; 0:succ ack; 1:succ nack
* \note
//        ___
// SCL  _|   |_
//      ___
// SDA     |___
*******************************************************************************/
int i2c_start(unsigned int addr)
{
    int result = 0;

    //slave address
    act_writel(addr, i2c_dev.reg_I2C_DAT);
    //start condition
    act_writel(I2C_ENABLE|I2C_PULLUP_ENABLE|I2C_GENERATE_START|I2C_RELEASE_BUS, i2c_dev.reg_I2C_CTL);

    result = _wait_tcb();
    if ( 0 == result )
    {
        result = _wait_ack();
    }

    return result;
}

/******************************************************************************/
/*!
* \par  Description:
*       i2c restart and transmit slave address
* \param[in]    unsigned int:i2c slave address
* \retval       int: -1:err; 0:succ ack; 1:succ nack
*******************************************************************************/
int i2c_restart(unsigned int addr)
{
    int result = 0;

    //slave address
    act_writel(addr, i2c_dev.reg_I2C_DAT);
    //restart condition
    act_writel(I2C_ENABLE|I2C_PULLUP_ENABLE|I2C_GENERATE_RESTART|I2C_RELEASE_BUS, i2c_dev.reg_I2C_CTL);

    result = _wait_tcb();
    if ( 0 == result )
    {
        result = _wait_ack();
    }

    return result;
}

/******************************************************************************/
/*!
* \par  Description:
*       i2c stop
* \param[in]    void
* \retval       void
* \note
//         ___
// SCL  __|
//           ___
// SDA  __ _|
*******************************************************************************/
void i2c_stop( void )
{
    unsigned int tmp;
    unsigned int times = i2c_wait_stop_time_out;

    //stop condition
    act_writel(I2C_ENABLE|I2C_PULLUP_ENABLE|I2C_GENERATE_STOP|I2C_RELEASE_BUS|I2C_GENERATE_NACK, i2c_dev.reg_I2C_CTL);
    //wait stop
    do
    {
        --times;
        udelay(1);

        tmp = act_readl(i2c_dev.reg_I2C_STAT);
    } while ( (0 == (tmp&I2C_STAT_STPD)) && (times > 0) );

    if (0 == times)
    {
        printk_err("\n\ntime out...........");
    }
#if 0
//用于调试超时时间
    else
    {
        printk("\n\nstop time:%d...............\n", times);
    }
#endif

    //clear stop bit
    act_writel(tmp|I2C_STAT_STPD, i2c_dev.reg_I2C_STAT);
}

/******************************************************************************/
/*!
* \par  Description:
*       i2c write one byte
*       不能单独使用, 需要配合start(), stop()
* \param[in]    unsigned char:
* \retval       int: -1:err; 0:succ ack; 1:succ nack
*******************************************************************************/
int i2c_write_byte(unsigned char byte)
{
    int result = 0;

    act_writel((unsigned int)byte, i2c_dev.reg_I2C_DAT);
    act_writel(I2C_ENABLE|I2C_PULLUP_ENABLE|I2C_RELEASE_BUS, i2c_dev.reg_I2C_CTL);

    result = _wait_tcb();
    if ( 0 == result )
    {
        result = _wait_ack();
    }

    return result;
}

/******************************************************************************/
/*!
* \par  Description:
*       i2c read one byte
*       不能单独使用, 需要配合start(), stop()
* \param[in]    unsigned char *: buffer pointer to store data.
* \param[in]    unsigned int: weather output ack, 1:ack; 0:nack
* \retval       int: 0:succ, <0:err
*******************************************************************************/
int i2c_read_byte(unsigned char *p_byte, unsigned int ack)
{
    int result = 0;

    if (ack == 1)
    {
        //realse bus and ack
        act_writel(I2C_ENABLE|I2C_PULLUP_ENABLE|I2C_RELEASE_BUS, i2c_dev.reg_I2C_CTL); 
    }
    else
    {
        //realse bus and ack
        act_writel(I2C_ENABLE|I2C_PULLUP_ENABLE|I2C_RELEASE_BUS|I2C_GENERATE_NACK, i2c_dev.reg_I2C_CTL); 
    }
    result = _wait_tcb();
    if (result < 0)
    {
        printk_err("\n\nread err...........\n");
        return -1;
    }
    *p_byte = (unsigned char)act_readl(i2c_dev.reg_I2C_DAT);

    return 0;
}

/******************************************************************************/
/*!
* \par  Description:
*       i2c write some bytes.
* \param[in]    unsigned int: slave address
* \param[in]    unsigned char *:buffer pointer that store some data ready to write.
* \param[in]    int: data count prepraring to write, counted by sizeof(int).
* \retval       int: 0:succ, <0:err
* \note
*       将start(), write_byte(), stop(),综合起来使用.
*       为通用函数, 可以任意控制write个数. 但不能涵盖所有i2c情况
*******************************************************************************/
int i2c_puts(unsigned int addr, unsigned char *buf, int cnt)
{
    int result = 0;

    if ( 0 == cnt )
    {
        return -1;
    }
    result = i2c_start( addr );
    if (result < 0)
    {
        printk_err("\n\nstart err\n..............");
        goto stop;
    }

    while (cnt > 0)
    {
        result = i2c_write_byte( *buf );
        if (result < 0)
        {
            printk_err("\n\nwrite err\n..............");
            goto stop;
        }

        cnt--;
        buf++;
    }

    stop:
    i2c_stop();

    return result;
}

/******************************************************************************/
/*!
* \par  Description:
*       i2c read some bytes
* \param[in]    unsigned int: slave address
* \param[in]    unsigned char *:buffer pointer to store data.
* \param[in]    int: data count prepraring to read, counted by sizeof(int).
* \retval       int: 0:succ, <0:err
* \note
*       将start(), write_byte(), stop(),综合起来使用.
*       为通用函数, 可以任意控制read个数. 但不能涵盖所有i2c情况
*******************************************************************************/
int i2c_gets(unsigned int addr, unsigned char *buf, int cnt)
{
    int result = 0;

    if ( 0 == cnt )
    {
        return -1;
    }

    result = i2c_start(addr);
    if (result < 0)
    {
        printk_err("\n\nstart err..............\n");
        goto stop;
    }

    while (cnt > 0)
    {
        //the last one byte, not ack
        if (cnt == 1)
        {
            result = i2c_read_byte(buf, 0);
        }
        else
        {
            result = i2c_read_byte(buf, 1);
        }
        if (result < 0)
        {
            printk_err("\n\nread err..............\n");
            goto stop;
        }

        cnt--;
        buf++;
    }

    stop:
    i2c_stop();

    return result;
}


/******************************************************************************/
/*!
* \par  Description:
*       i2c write one bytes.
* \param[in]    unsigned int: slave address
* \param[in]    unsigned char *:buffer pointer that store some data ready to write.
* \param[in]    int: data count prepraring to write, counted by sizeof(int).
* \retval       int: 0:succ, <0:err
* \note
*       将start(), write_byte(), stop(),综合起来使用.
*       为通用函数, 可以任意控制write个数. 但不能涵盖所有i2c情况
*******************************************************************************/
int i2c_write_one_reg(unsigned int i2c_addr, unsigned int reg_addr, unsigned int *data_buf)
{
    int result = 0;

    result = i2c_start( i2c_addr );
    if (result < 0)
    {
        printk_err("\nstart err..............");
        goto stop;
    }

    result = i2c_write_byte((unsigned char)(reg_addr&0xFF));
    if (result < 0)
    {
        printk_err("\nwrite1 err..............");
        goto stop;
    }

    result = i2c_write_byte((unsigned char)((*data_buf>>8)&0xFF));
	
    if (result < 0)
    {
        printk_err("\nwrite2 err..............");
        goto stop;
    }

    result = i2c_write_byte((unsigned char)(*data_buf&0xFF));

	// printf("++++++++++++++i2c_write_one_reg+++++++++result=%d\n",result);
    if (result < 0)
    {
        printk_err("\nwrite3 err..............");
        goto stop;
    }   

    stop:
    i2c_stop();

    return result;
}

/******************************************************************************/
/*!
* \par  Description:
*       i2c read some bytes
* \param[in]    unsigned int: slave address
* \param[in]    unsigned char *:buffer pointer to store data.
* \param[in]    int: data count prepraring to read, counted by sizeof(int).
* \retval       int: 0:succ, <0:err
* \note
*       将start(), write_byte(), stop(),综合起来使用.
*       为通用函数, 可以任意控制read个数. 但不能涵盖所有i2c情况
*******************************************************************************/
int i2c_read_one_reg(unsigned int i2c_addr, unsigned int reg_addr,unsigned int *data_buf)
{
    int result = 0;
    unsigned int val=0;
    unsigned char tmp =0;

    result = i2c_start(i2c_addr);
    if (result < 0)
    {
        printk_err("\nstart err..............");
        goto stop;
    }

    result = i2c_write_byte((unsigned char)(reg_addr&0xFF));
    if (result < 0)
    {
        printk_err("\nwrite err..............");
        goto stop;
    }
  
  //  i2c_stop();
   //  mdelay(3);	
    result = i2c_restart(i2c_addr+1);
    if (result < 0)
    {
        printk_err("\nrestart err..............");
        goto stop;
    }

    result = i2c_read_byte(&tmp, 1);
    if (result < 0)
    {
        printk_err("\nread err..............");
        goto stop;
    }
    val =  (unsigned int)tmp<<8;
//	printf("+++++0001111++++val=%x\n",val);

    result = i2c_read_byte(&tmp, 0);
    if (result < 0)
    {
        printk_err("\nread err..............");
        goto stop;
    }
    val = val + tmp;

	//printf("+++++000222++++val=%x\n",val);
	
    *data_buf = val;
    
    stop:
  //  mdelay(2);			
    i2c_stop();
	//printf("+++++0001111++++result=%x\n",result);

  //  return val;
	return result;
}

static void _fm_clk_cb(unsigned int clk, curclk_t *curclk, void *dev_id, unsigned int cmd, unsigned int direction)
{
    unsigned int apb_clk;
    unsigned int div;

    if(direction == CHANGEPLL_UP)
    {
        if(cmd == CHANGEPLL_START)
        {
            apb_clk = curclk->corepll / (curclk->cclk_div * curclk->sclk_div * curclk->pclk_div);

            //apb_clk单位:M, FM_CFG_I2C_CLK单位:1K, 所以需要*1000
            div = (apb_clk * 1000 + ( FM_CFG_I2C_CLK - 1 )) / FM_CFG_I2C_CLK;
            div = (div + 15) >> 4;   // div/16
            
            i2c_change_clk_div( div );
        }
    }
    else
    {
        if(cmd == CHANGEPLL_END)
        {
            apb_clk = curclk->corepll / (curclk->cclk_div * curclk->sclk_div * curclk->pclk_div);

            //apb_clk单位:M, FM_CFG_I2C_CLK单位:1K, 所以需要*1000
            div = (apb_clk * 1000 + ( FM_CFG_I2C_CLK - 1 )) / FM_CFG_I2C_CLK;
            div = (div + 15) >> 4;   // div/16

            i2c_change_clk_div( div );
        }
    }
}
/******************************************************************************/
/*!
* \par  Description:
*       i2c Initialization, Request some i2c related resource.
* \param[in]    int: 0, use i2c_1;  1, use i2c_2.
* \param[in]    unsigned int: i2c clock, unit: KHz
* \retval       int: -1:err; 0:succ
*******************************************************************************/
int i2c_init(int port, unsigned int i2c_clk)
{
    int result = 0;
    unsigned int apb_clk;
    unsigned int div;

    i2c_get_gpio(port);
    if(get_ic_type() == 0x09)
    {
        if(p_request_clkadjust == NULL)
        {
            p_request_clkadjust = kernel_sym( "request_clkadjust" );
        }
        if(p_free_clkadjust == NULL)
        {
            p_free_clkadjust = kernel_sym( "free_clkadjust" );
        }
        if(p_get_clock == NULL)
        {
            p_get_clock = kernel_sym( "get_clock" );
        }
        if(p_request_clkadjust != NULL)
        {
            p_request_clkadjust(CLKCB_ID_I2C, _fm_clk_cb, CLKCB_NAME_I2C);
        }
    }
    result = multip_get_pinowner(MOD_I2C,PIN_WAIT_CAN_SLEEP,0,0);//该函数会确保pin
    if(result != 0 )
    {
        printk_err("\n\nget MOD_I2C pin err..........\n");
        return result;
    }

    //参数检查, 如果非法, 使用默认值
    if ( 0 == i2c_clk )
    {
        i2c_clk = 100;
    }

    //open I2C CLK
    enable_module_clk(MOD_I2C);

    switch (port)
    {
        case 0:
        {
            i2c_dev.reg_I2C_CTL = I2C_CTL;
            i2c_dev.reg_I2C_CLKDIV = I2C_CLKDIV;
            i2c_dev.reg_I2C_STAT = I2C_STAT;
            i2c_dev.reg_I2C_ADDR = I2C_ADDR;
            i2c_dev.reg_I2C_DAT = I2C_DAT;
            break;
        }
        case 1:
        {
            i2c_dev.reg_I2C_CTL = I2C_CTL;
            i2c_dev.reg_I2C_CLKDIV = I2C_CLKDIV;
            i2c_dev.reg_I2C_STAT = I2C_STAT;
            i2c_dev.reg_I2C_ADDR = I2C_ADDR;
            i2c_dev.reg_I2C_DAT = I2C_DAT;
            break;
        }

        default:
        {
            return -1;
        }
    }

    i2c_dev.i2c_port = (unsigned int) port;

    i2c_reset();
    
    //计算相关的超时时间. i2c_clk: KHz转换成超时周期us, *保险系数4
    i2c_wait_tcb_time_out  = (unsigned int)(1000 / i2c_clk) * 9 * 4;   //tcb一般9个clk
    i2c_wait_stop_time_out = (unsigned int)(1000 / i2c_clk) * 2 * 4;   //stop一般1.5-2个clk
    if(get_ic_type() == 0x09)
    {
        apb_clk = _apb_clk_get();
        if(p_get_clock != NULL)
        {
            p_get_clock(GET_PCLK, &apb_clk); 
        }
        //apb_clk单位:M, i2c_clk:1K, 所以需要*1000
        div = ((apb_clk * 1000 + i2c_clk - 1) / i2c_clk);
        div = (div + 15) >> 4;  // div/16
    }
    else
    {
        //5013 I2C使用独立的24M时钟源
        div =  (24 *1000 / i2c_clk + i2c_clk - 1) / 16;
    }
    i2c_change_clk_div( div );
    
    return 0;
}

/******************************************************************************/
/*!
* \par  Description:
*       i2c exit, release some i2c related resource.
* \param[in]    void
* \retval       int: -1:err; 0:succ
*******************************************************************************/
int i2c_exit(void)
{
    switch (i2c_dev.i2c_port)
    {
        case 0:
        case 1:
        {
            act_writel(0x00000000, i2c_dev.reg_I2C_CTL);
            break;
        }

        default:
        {
            return -1;
        }
    }

    disable_module_clk(MOD_I2C);
    multip_release_pinowner(MOD_I2C,PIN_WAIT_CAN_SLEEP);
    i2c_release_gpio((int)i2c_dev.i2c_port);
    if(get_ic_type() == 0x09)
    {
        if(p_free_clkadjust != NULL)
        {
            p_free_clkadjust( CLKCB_ID_I2C, CLKCB_NAME_I2C );
        }
    }
    
    return 0;
}

#endif

