/********************************************************************************
 *                              这里填写项目名
 *                            Module: FM Drvier
 *                 Copyright(c) 2001-2007 Actions Semiconductor,
 *                            All Rights Reserved.
 *
 * History:
 *      <author>    <time>           <version >             <desc>
 *      dengjihai   2010-11-27 09:00     1.0             build this file
 ********************************************************************************/
/*!
 * \file   fm_hw_module.c
 * \brief
 * \author dengjihai
 * \par GENERAL DESCRIPTION:
 *       In This File
 *       1)using i2c bus to control FM IC.
 *       2)all the code need to rewrite while using different FM IC
 * \par EXTERNALIZED FUNCTIONS:
 *     Copyright(c) 2001-2007 Actions Semiconductor, All Rights Reserved.
 *
 *  \version 1.0
 *  \date  2008/7/3
 *******************************************************************************/

#include "include_fm.h"

static int _fm_set_freq_no_check(int freq);
static int _fm_set_mute(int mute);
static int _fm_set_tune(int tune);

////register information
/*---------------------------00h--------------------*/
//set register:00h 
//xo funtion
//1 = using  AR1000 XO function 0 = using External 32.768KHz Reference Clock
#define SET_XO_BIT              (0x07)
#define SET_XO_MASK             (0x01)
#define SET_XO_ENABLE           (0x01)
#define SET_XO_DISABLE          (0x00)
//enable
//0 disable, 1 enable
#define SET_ENABLE_BIT          (0x00)
#define SET_ENABLE_MASK         (0x01)
#define SET_ENABLE_DISABLE      (0x00)
#define SET_ENABLE_ENABLE       (0x01)

/*---------------------------01h--------------------*/
//set register:01h 
//mono select
//0 stereo, 1 force mono
#define SET_MONO_BIT            (0x03)
#define SET_MONO_MASK           (0x01)
#define SET_MONO_STEREO         (0x00)
#define SET_MONO_MONO           (0x01)

//mute disable
//0 mute, 1 normal operation
#define SET_HMUTE_BIT           (0x01)
#define SET_HMUTE_MASK          (0x01)
#define SET_HMUTE_MUTE          (0x00)
#define SET_HMUTE_NORMAL        (0x01)

/*---------------------------02h--------------------*/

//tune enable
//0 disable, 1 enable
#define SET_TUNE_MODE_BIT       (0x09)
#define SET_TUNE_MODE_MASK      (0x01)
#define SET_TUNE_MODE_DISABLE   (0x00)
#define SET_TUNE_MODE_ENABLE    (0x01)
//channel select
#define SET_CHAN_BIT            (0x00)
#define SET_CHAN_MASK           (0x1ff)

/*---------------------------03h--------------------*/
//03h
//seek direction
//0 seek down, 1 seek up
#define SET_SEEK_DIR_BIT        (0x0f)
#define SET_SEEK_DIR_MASK       (0x01)
#define SET_SEEK_DIR_DOWN       (0x00)
#define SET_SEEK_DIR_UP         (0x01)
       
//seek start
//0 disable/stop seek, 1 enable seek
#define SET_SEEK_START_BIT      (0x0e)
#define SET_SEEK_START_MASK     (0x01)
#define SET_SEEK_STOP           (0x00)
#define SET_SEEK_START          (0x01)

//channel spacing
//1 100KHZ, 0 200KHZ, 
#define SET_SPACE_BIT            (0x0d)
#define SET_SPACE_MASK           (0x01)
#define SET_SPACE_100KHZ         (0x01)
#define SET_SPACE_200KHZ         (0x00)
//band select
//0/1 87.5~108MHZ, 2 76~91MHZ, 3 76~108MHZ
#define SET_BAND_MODE_BIT        (0x0b)
#define SET_BAND_MODE_MASK       (0x03)
#define SET_BAND_MODE_US         (0x00)
#define SET_BAND_MODE_JAPAN      (0x10)
#define SET_BAND_MODE_JAPAN_WIDE (0x11)
//seek threshold
#define SET_SEEK_THRESHOLD_BIT   (0x00)
#define SET_SEEK_THRESHOLD_MASK  (0x7f)
/*--------------------------------------------------*/

/*-------------------------12h--------------------*/
//RSSI
#define READ_RSSI_BIT            (0x09)
#define READ_RSSI_MASK           (0x7f)
//IF_CNT
#define READ_IF_CNT_BIT          (0x00)
#define READ_IF_CNT_MASK         (0x1ff)
/*------------------------end-----------------------*/
/*---------------------------13h--------------------*/
//read channel
#define READ_CHAN_BIT            (0x07)
#define READ_CHAN_MASK           (0x1ff)

//seek/tune complete
//0  not complete, 1 complete
#define READ_STC_BIT             (0x05)
#define READ_STC_MASK            (0x01)

//seek fail
//0 seek success, 1 seek failure
#define READ_SF_BIT              (0x04)
#define READ_SF_MASK             (0x01)

//stereo indicator
//0 mono, 1 stereo
#define READ_SI_BIT              (0x03)
#define READ_SI_MASK             (0x01)
/*--------------------------------------------------*/



////fm module I2C address
#define I2C_OPERATION_ADDR       (0xc8)
#define I2C_READ_ADDR            (0xc9)
#define I2C_WRITE_ADDR           (0xc8)
//ar module read/write reg addr
#define AR_READ_REG_ADDR         (0xc9) 
#define AR_WRITE_REG_ADDR        (0xc8)

#define    AR_WRITE_REG_ADDR_6222  0x00

#define IF_HIGH                  (250+27)
#define IF_LOW                   (250-27)

////fm threshold setting,should decide threshold self
#define SEEK_THRESHOLD_LOW       (15)
#define SEEK_THRESHOLD_MID       (20)
#define SEEK_THRESHOLD_HIGH      (25)

////To be set to FM module to parameter setting
/*static unsigned int Write_Buffer[18] =
{ 
#if FM_CFG_EXTERAL_CLK > 0
    0xFF7B, //00h,R0
#else   
    0xFFFB, //00h,R0
#endif    
    0x5B15, //01h,R1
    0xD0B9, //02h,R2
    0xA010, //03h,R3
    0x0780, //04h,R4
    0x28AB, //05h,R5
    0x6400, //06h,R6
    0x1EE7, //07h,R7
    0x7141, //08h,R8
    0x007D, //09h,R9
    0x82C6, //0ah,R10
    0x4E55, //0bh,R11
    0x970C, //0ch,R12
    0xB845, //0dh,R13
    0xFC2D, //0eh,R14
    0x8097, //0fh,R15
    0x04A1, //10h,R16
    0xDF6A  //11h,R17 
};*/

static unsigned int Write_Buffer[11] =
{ 



0x16AA,  // 0x00  Power off
0x96AA,  //0x00   Power on
0x4008,  //0x02    vol = 0a ,disable mute
0x1900,  //0x04   //enable rclk osc
0x650f, //0x05  //set seek noise th and spike th ,0x01h=0010bin,00=80(noise_th),01=144(spike_th),and rssi th=0x0c.
0xB400, //0x07   //正弦波
0xB005, //0x06    24M 共享
0x043C, ////0x08  set seek band of freq  start_freq0x043C=1084. x100khz
0x035C, ////0x09  set seek start_freq 0x035C=860, x100khz
0x0B10,////0x11   rclk force 0x0Bh= bin1011
0x1108//0x1108//0x1008,////0x20   1008 hopping open is better than 1018 nohopping







};
////Get value from FM module
static unsigned int Read_Buffer[11] =
{ 
    0x0000, //12h,RSSI
    0x0000, //13h,STATUS
    0x0000, //14h,RBS
    0x0000, //15h,RDS1
    0x0000, //16h,RDS2
    0x0000, //17h,RDS3
    0x0000, //18h,RDS4
    0x0000, //19h,RDS5
    0x0000, //1ah,RDS6
    0x0000, //1bh,DEVID
    0x0000, //1ch,CHIPID
};

//value to be set
static struct tagSetValue{
    int high_z;         //audio output high-z: 0 high impedance, 1 normal operation
    int hmute;           //mute setting: 0 mute, 1 normal operation
    int seek_dir;       //seek direction: 0 seek down, 1 seek up
    int seek_start;     //seek start: 0 disable/stop seek, 1 seek start
    int enable;         //enable: 0 disable, 1 enable    
    int freq;           //frequency to be set
    int tune_start;     //tune start: 0 disable/stop, 1 start
    int band;           //band mode: 0 US, 1 JAPAN
    int space;           //channel space: 1 = 100KHZ,0 = 200KHZ
    int threshold;      //seek threshold
}set_value;

//value read
static struct tagReadValue{
    int complete_flag;  //complete flag: 0 not complete, 1 complete
    int fail_flag;      //seek fail flag: 0 seek successful, 1 seek failure
    int freq;           //frequency got
    int stereo;         //stereo mode:  0 mono, 1 stereo
    int intensity;      //IF counter result
    int tune_true;      //is a station or not: 0 not a station, 1 a station
    int if_cnt;
}read_value;

//function declaration here
static int _is_transmit_ok(void);
static int _is_receive_ok(void);
static int _is_communicate_ok(void);

/******************************************************************************
** Description:
**     get chan from freq or get freq from chan 
** Param:
**     None
** Return:
**     None
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _chan_to_freq(int chan)
{
    int freq=0;

    freq = ( chan + 690) * 100;
    return freq;
}

static int _freq_to_chan(int freq)
{
    int chan=0;
       
    chan = ( freq - 69000 ) / 100;
    if(chan < 0)
    {
        chan = 0;
    }
    
    return chan;    
}



////////////////////////////////////////////////////////////////



void OperationRTC6222_write(unsigned short in_data, unsigned char reg_offset)
{
     unsigned int Write_Buffer1[0];
	 
             Write_Buffer1[0]=in_data;

  //   printf("+++++0001++++in_data=%x\n,reg_offset=%x\n",in_data,reg_offset);

	_write_one_reg( reg_offset, &Write_Buffer1[0]);



}
//#pragma location="FM_SEG_RTC6222"
unsigned short OperationRTC6222_read(unsigned char reg_offset)
{


unsigned short reg_value = 0;  
unsigned int Read_Buffer[0];
Read_Buffer[0]=0x0000;

reg_value =_read_one_reg(reg_offset,&Read_Buffer[0]);

//printf("+++++000222++++reg_value=%x\n,Read_Buffer[0]=%x\n",reg_value,Read_Buffer[0]);

return Read_Buffer[0];


}



//////////////////////////////////////////////////////////





/******************************************************************************
** Description:
**     Read one frame: FM Module read one frame every time
** Param:
**     None
** Return:
**     0 success
**     1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _read_one_frame(void)
{
    int result = -1;
    unsigned int i = 0;
    
    result = i2c_init( I2C_CHAN_NUM, FM_CFG_I2C_CLK );
    if ( 0 == result )
    {
        for( i = 0; i < (sizeof(Read_Buffer)/sizeof(Read_Buffer[0])); i++ )
        {
            result = i2c_read_one_reg(I2C_OPERATION_ADDR, AR_READ_REG_ADDR + i, &(Read_Buffer[i]));
        }
    }
    i2c_exit();
    
    return result;
}

/******************************************************************************
** Description:
**     Write one frame: FM Module write one frame every time
** Param:
**     None
** Return:
**     0 success
**     1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _write_one_frame(void)
{
    int result = -1;
    unsigned int i = 0;
    
    result = i2c_init( I2C_CHAN_NUM, FM_CFG_I2C_CLK );
    if ( 0 == result )
    {
        for( i = 1; i < (sizeof(Write_Buffer)/sizeof(Write_Buffer[0])); i++ )
        {
            result = i2c_write_one_reg(I2C_OPERATION_ADDR, AR_WRITE_REG_ADDR + i, &Write_Buffer[i] );
            if( result < 0 )
            {
                break;
            }
        }
        i2c_write_one_reg(I2C_OPERATION_ADDR, AR_WRITE_REG_ADDR + 0, &Write_Buffer[0] );
    }
    i2c_exit();
    
    return result;
}
/******************************************************************************
** Description:
**     Read one frame: FM Module read 5 bytes(one frame) every time
** Param:
**     None
** Return:
**     0 success
**     1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
int _read_one_reg(unsigned int reg_addr,unsigned int *val)
{
    int result = -1;
    int i = 0;
	
//    printf("++++++++++++++002+++++++++_read_one_reg");
	
    result = i2c_init( I2C_CHAN_NUM, FM_CFG_I2C_CLK );

	
    if( 0 == result )
    {
        for(i = 0; i < 3; i++ )
        {
            result = i2c_read_one_reg(I2C_OPERATION_ADDR, reg_addr, val);
            if( result >= 0 )
            {
                break;
            }
        }
    }
    i2c_exit();   
//   printf("++++++++++++++002+++++++++result=%d\n",result);
    return result;
}

/******************************************************************************
** Description:
**     Write one frame: FM Module write 5 bytes(one frame) every time
** Param:
**     None
** Return:
**     0 success
**     1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
int _write_one_reg(unsigned int reg_addr, unsigned int *val)
{
    int result = -1;
    int i = 0;
 
    result = i2c_init( I2C_CHAN_NUM, FM_CFG_I2C_CLK );
	// printf("++++++++++++++002+++++++++result=%d\n",result);
    if( 0 == result )
    {
        for(i = 0; i < 3; i++ )
        {
            result = i2c_write_one_reg(I2C_OPERATION_ADDR, reg_addr, val);
            if( result >= 0 )
            {
                break;
            }
        }
    }
    i2c_exit();  	
  
    return result;
}

//此函数只写需要变更的寄存器的值，在AR模组中，主要是0x01,0x02,0x03寄存器
int _write_opration(void)
{
    int result = 0;
    
    result = _write_one_reg(AR_WRITE_REG_ADDR + 0x01, &Write_Buffer[1]);
    if( result < 0 )
    {
        printk_err("write reg error\n");
        return result;
    }
    result = _write_one_reg(AR_WRITE_REG_ADDR + 0x02, &Write_Buffer[2]);
    if( result < 0 )
    {
        printk_err("write reg error\n");
        return result;
    }
    result = _write_one_reg(AR_WRITE_REG_ADDR + 0x03, &Write_Buffer[3]);
    if( result < 0 )
    {
        printk_err("write reg error\n");
        return result;
    }
    
    return result;
}
//此函数只读需要的寄存器的值，在AR模组中，主要是0x12,0x13寄存器
int _read_opration(void)
{
    int result = 0;
    
    result = _read_one_reg(AR_READ_REG_ADDR,&Read_Buffer[0]);
    if( result < 0 )
    {
        printk_err("write reg error\n");
        return result;
    }
    result = _read_one_reg(AR_READ_REG_ADDR + 0x01,&Read_Buffer[1]);
    if( result < 0 )
    {
        printk_err("write reg error\n");
        return result;
    }
    
    return result;
}

/******************************************************************************
** Description:
**     Assemble write buffer
** Param:
**     None
** Return:
**     None
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static void _assemble_write_buffer(void)
{
    int temp=0;
    unsigned int chan=0;

    //reg 00h
    //enable setting
    temp = (int)Write_Buffer[0];
    temp &= ~(SET_ENABLE_MASK << SET_ENABLE_BIT);
    if(set_value.enable == 0)
    {
        temp |= (SET_ENABLE_DISABLE & SET_ENABLE_MASK) << (SET_ENABLE_BIT);
    }
    else
    {
        temp |= (SET_ENABLE_ENABLE & SET_ENABLE_MASK) << (SET_ENABLE_BIT);
    }
    Write_Buffer[0] = (unsigned int)temp;
    //reg 01h
    //hmute setting
    temp = (int)Write_Buffer[1];
    temp &= ~(SET_HMUTE_NORMAL << SET_HMUTE_MASK);
    if(set_value.hmute == 0)
    {
        temp |= (SET_HMUTE_MUTE & SET_HMUTE_MASK) << (SET_HMUTE_BIT);
    }
    else
    {
        temp |= (SET_HMUTE_NORMAL & SET_HMUTE_MASK) << (SET_HMUTE_BIT);
    }
    Write_Buffer[1] = (unsigned int)temp;
    
    //reg 02h
    temp = (int)Write_Buffer[2];
    //chan setting
    chan = (unsigned int)_freq_to_chan(set_value.freq);
    temp &= ~(SET_CHAN_MASK << SET_CHAN_BIT);
    temp |= (int)(chan >> SET_CHAN_BIT) & (SET_CHAN_MASK);
    //tune setting
    temp &= ~(SET_TUNE_MODE_MASK << SET_TUNE_MODE_BIT);
    if(set_value.tune_start == 0)
    {
        temp |= (SET_TUNE_MODE_DISABLE & SET_TUNE_MODE_MASK) << (SET_TUNE_MODE_BIT);
    }
    else
    {
        temp |= (SET_TUNE_MODE_ENABLE & SET_TUNE_MODE_MASK) << (SET_TUNE_MODE_BIT);
    }
    Write_Buffer[2] = (unsigned int)temp;

    //reg 03h
    temp = (int)Write_Buffer[3];
    //seek direction setting
    temp &= ~(SET_SEEK_DIR_MASK << SET_SEEK_DIR_BIT);
    if(set_value.seek_dir == 0)
    {
        temp |= (SET_SEEK_DIR_DOWN & SET_SEEK_DIR_MASK) << (SET_SEEK_DIR_BIT);
    }
    else
    {
        temp |= (SET_SEEK_DIR_UP & SET_SEEK_DIR_MASK) << (SET_SEEK_DIR_BIT);
    }
    
    //seek start setting
    temp &= ~(SET_SEEK_START_MASK << SET_SEEK_START_BIT);
    if(set_value.seek_start == 0)
    {
        temp |= (SET_SEEK_STOP & SET_SEEK_START_MASK) << (SET_SEEK_START_BIT);
    }
    else
    {
        temp |= (SET_SEEK_START & SET_SEEK_START_MASK) << (SET_SEEK_START_BIT);
    } 
    //spacing
    temp &= ~(SET_SPACE_MASK << SET_SPACE_BIT);
    if(set_value.space == 100)
    {
        temp |= (SET_SPACE_100KHZ & SET_SPACE_MASK) << (SET_SPACE_BIT);
    }
    else if(set_value.space == 200)
    {
        temp |= (SET_SPACE_200KHZ & SET_SPACE_MASK) << (SET_SPACE_BIT);
    }
    else
    {
        //default space is 100;
        temp |= (SET_SPACE_100KHZ & SET_SPACE_MASK) << (SET_SPACE_BIT);
    }
    //band select
    temp &= ~(SET_BAND_MODE_MASK << SET_BAND_MODE_BIT);
    if(set_value.band == 0)
    {
        temp |= (SET_BAND_MODE_US & SET_BAND_MODE_MASK) << (SET_BAND_MODE_BIT);
    }
    else
    {
        temp |= (SET_BAND_MODE_JAPAN & SET_BAND_MODE_MASK) << (SET_BAND_MODE_BIT);
    }
    //threshold
    temp &= ~(SET_SEEK_THRESHOLD_MASK << SET_SEEK_THRESHOLD_BIT);
    temp |= (set_value.threshold & SET_SEEK_THRESHOLD_MASK) << (SET_SEEK_THRESHOLD_BIT);
    Write_Buffer[3] = (unsigned int)temp;
}

/******************************************************************************
** Description:
**     Disassemble read buffer
** Param:
**     None
** Return:
**     None
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static void _disassemble_read_buffer(void)
{
    unsigned int temp=0;
    int chan=0;
    
    ///////////1st byte
    temp = Read_Buffer[0];
    read_value.intensity =(int) ((temp >> READ_RSSI_BIT) & (READ_RSSI_MASK));
    read_value.if_cnt = (int) (temp >> READ_IF_CNT_BIT) & (READ_IF_CNT_MASK);

    temp = Read_Buffer[1];
    chan = (int) (temp >> READ_CHAN_BIT) & READ_CHAN_MASK;
    read_value.freq = _chan_to_freq(chan);
    
    read_value.complete_flag = (int) (temp >> READ_STC_BIT) & (READ_STC_MASK);
    read_value.fail_flag = (int) (temp >> READ_SF_BIT) & (READ_SF_MASK);
    read_value.stereo = (int) (temp >> READ_SI_BIT) & (READ_SI_MASK);
}

/******************************************************************************
** Description:
**     FM input to analog mixer mute
** Param:
**     mute      1 mute, 0 not mute
** Return:
**     None
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static void _ada_set_mute(int mute)
{
/*  
    //驱动中不宜直接操作DAC_ANALOG1寄存器，以免引起啪声
    if (mute == 0)
    {
        act_writel(act_readl(DAC_ANALOG1)|(0x1<<9),DAC_ANALOG1);   //undo   
    }
    else
    {
        act_writel(act_readl(DAC_ANALOG1)&(~(0x1<<9)),DAC_ANALOG1); 
    }
*/    
}

/******************************************************************************
** Description:
**     to check whether transmit success or not
** Param:
**     None
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _is_transmit_ok(void)
{
    int flag=-1;
    int c=0;
    
    for (c = 3; c > 0; c--)
    {
        flag = _write_one_frame();
        if (flag == 0)
        {
            break;
        }
    }
    
    return (flag);
}

/******************************************************************************
** Description:
**     to check whether receive success or not
** Param:
**     None
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _is_receive_ok(void)
{
    int flag=-1;
    int c=0;
    
    mdelay(30);
    
    for (c = 3; c > 0; c--)
    {
        flag = _read_one_frame();
        if (flag == 0)
        {
            break;
        }
    }
    
    return (flag);
}

/******************************************************************************
** Description:
**     to check whether transmit and receive success or not
** Param:
**     None
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _is_communicate_ok(void)
{
    int flag=-1;
/*    
    flag = _is_transmit_ok();
    
    if(flag == -1)
    {
        return -1;
    }
    
    flag = _is_receive_ok();

    if (flag == -1)
    {
        return -1;
    }
 
  //  flag = _write_opration();
    if(flag < 0)
    {
        return -1;        
    }
    mdelay(50);
    flag = _read_opration();
    if(flag < 0)
    {
        return -1;        
    }
    */ 
    return (flag);
}

/******************************************************************************
** Description:
**     To check freq valid or not
** Param:
**     freq      the frequency to check
** Return:
**     0 valid, -1 not valid
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _is_freq_valid(int freq)
{
    int band_limit_low=0, band_limit_high=0;
    
    if(set_value.band == 1)
    {
        band_limit_low = 76000;
        band_limit_high = 90000;
    }
    else
    {
        band_limit_low = 87000;
        band_limit_high = 108000;
    }
    
    if((freq < band_limit_low) || (freq > band_limit_high))
    {
        return -1;
    }
    else
    {
        return 0;
    }
}

/******************************************************************************
** Description:
**     To check freq reach band limit
** Param:
**     freq      the frequency to check
** Return:
**     0 not reached, 1 reached
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _is_freq_reach_band_limit(int freq)
{
    int band_limit_low=0, band_limit_high=0;
    
    if(set_value.band == 1)
    {
        band_limit_low = 76000;
        band_limit_high = 90000;
    }
    else
    {
        band_limit_low = 87000;
        band_limit_high = 108000;
    }
    
    if( ((set_value.seek_dir == 1) && (freq >= band_limit_high)) 
        || ((set_value.seek_dir == 0) && (freq <= band_limit_low)) )
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

#pragma __PRQA_IGNORE_START__
/******************************************************************************
** Description:
**     Break auto seek station
** Param:
**     cur_radio_info      return current seek information to top
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _fm_seek_break(struct radio_info_t *cur_radio_info)
{
    int flag = -1;
    
    set_value.seek_start = 0;
    
   // _assemble_write_buffer();
  //  flag = _write_opration();
    _fm_set_mute(0);
    
    return flag;      
}

/******************************************************************************
** Description:
**     Start auto seek station
** Param:
**     seek_info      seek parameters 
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _fm_seek_start(struct radio_seek_info_t * seek_info)
{
    int flag=-1;

    printk_dbg("【FM】start seek: %d\n",seek_info->search_freq);
    //1.set hmute bit
//    _fm_set_mute(1);
//    mdelay(100);
    //2.clear tune bit
    _fm_set_tune(0);
    //3.set chan bit
    set_value.freq = seek_info->search_freq;
    flag = _is_freq_valid(set_value.freq);
    if(flag < 0)
    {
        printk_err();
        return -1;
    }
    //4.clear seek bit
/*    set_value.seek_start = 0;
    _assemble_write_buffer();
  //  flag = _write_opration();
    if( flag < 0 )    
    {
        printk_err("write opration fail");
        return -1;
    }
    //5.set seekup/space/band/seekth bits
    set_value.seek_dir = (int)seek_info->search_direction;
    _assemble_write_buffer();
    flag = _write_one_reg(AR_WRITE_REG_ADDR + 0x03, &Write_Buffer[3]);
    if( flag < 0 )    
    {
        printk_err("write opration fail");
        return -1;
    }
    //6.enable seek bit
    set_value.seek_start = 1;
    _assemble_write_buffer();
    flag = _write_one_reg(AR_WRITE_REG_ADDR + 0x03, &Write_Buffer[3]);
*/
    mdelay(10);
    
    return flag;
}

/******************************************************************************
** Description:
**     Get seek status information
** Param:
**     radio_info_check   return radio information to top level
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _fm_check_seek_status(struct radio_info_t * radio_info_check)
{
    int flag=-1;
    int band_limit_low=0, band_limit_high=0;
    
 /*   flag = _read_opration();
    if(flag == -1)
    {
        printk_err();
        return -1;
    }
    _disassemble_read_buffer(); */

    
    radio_info_check->current_freq = read_value.freq;
    radio_info_check->intensity = read_value.intensity;
    radio_info_check->stereo_status = read_value.stereo; 

    if(read_value.complete_flag == 1)
    {
        _fm_set_mute(0);
        flag = _is_freq_reach_band_limit(read_value.freq);
        if(flag == 1)
        {
            if(set_value.band == 1)
            {
                band_limit_low = 76000;
                band_limit_high = 90000;
            }
            else
            {
                band_limit_low = 87000;
                band_limit_high = 108000;
            }
            if( set_value.seek_dir == 1 )
            {
                read_value.freq = band_limit_high;
            }
            else
            {
                read_value.freq = band_limit_low;
            }
            radio_info_check->current_freq = read_value.freq;
            radio_info_check->seek_status = REACH_BANDLIMIT;            
        }
        else
        {
            printk_dbg("【FM】intensity:%d\n",read_value.intensity);
#if FM_CFG_STEREO_CHECK > 0
            //tune_true 此标志位一直为0，不会出现1的情况
            if((read_value.intensity >= set_value.threshold)
                && ((read_value.if_cnt <= IF_HIGH) && (read_value.if_cnt >= IF_LOW))
                &&(read_value.stereo > 0))
#else
            if((read_value.intensity >= set_value.threshold) )
             //   && ((read_value.if_cnt <= IF_HIGH) && (read_value.if_cnt >= IF_LOW)))
#endif                
            {
                printk_dbg("【FM】found a station");
                radio_info_check->seek_status = FOUND_STATION;
            }
            else
            {
                radio_info_check->seek_status = INVALID_STATION;
            }
        }
        set_value.seek_start = 0;
    }
    else
    {
        radio_info_check->seek_status = FOUND_NOSTAION;
    }        
   
    return 0;    
}

/******************************************************************************
** Description:
**     tune the freqency to check whether is a valid station 
** Param:
**     seek_info      seek parameters 
** Return:
**     >=0 tune status
**     -1  failed
** Example code:
**     \code
**           list example code in here
**     \endcode
** Algorithm:
**     
*******************************************************************************/
static int _fm_tune_start(struct radio_seek_info_t *seek_info)
{
    int flag=-1;
    int tune_status=-1;

  /*  flag = _is_freq_valid(seek_info->search_freq);
    if(flag < 0)
    {
        printk_err();
        return -1;
    }

    _ada_set_mute(1);
    
    //1.set hmute bit
//    _fm_set_tune(1);
//    mdelay(100);
    //2.clear tune bit
    set_value.tune_start = 0;
    _assemble_write_buffer();
    flag = _write_one_reg(AR_WRITE_REG_ADDR + 0x02, &Write_Buffer[2]);
    if( flag < 0 )
    {
        printk_err();
        _ada_set_mute(0);
        return -1;        
    }
    //3.clear seek bit
    //4.set band/space/chan bit   
    set_value.freq = seek_info->search_freq;
    _assemble_write_buffer();
   // flag = _write_opration();   
    if(flag < 0)
    {
        printk_err();
        _ada_set_mute(0);
        return -1;        
    }
    //5.enable tune bit
    set_value.tune_start = 1;
    set_value.seek_dir = (int)seek_info->search_direction;
    
    _assemble_write_buffer();
//    flag = _write_opration();   
    if(flag < 0)
    {
        printk_err();
        _ada_set_mute(0);
        return -1;        
    }
    //6.clear hmute bit
    _fm_set_tune(0);

    mdelay(30);
    flag = _read_opration();   
    if(flag < 0)
    {
        printk_err();
        _ada_set_mute(0);
        return -1;        
    }
    _disassemble_read_buffer();
    
    if(read_value.complete_flag == 1)
    {
        flag = _is_freq_reach_band_limit(read_value.freq);
        if(flag == 1)
        {
            tune_status = REACH_BANDLIMIT;
        }
        else
        {
#if FM_CFG_STEREO_CHECK > 0
            //tune_true 此标志位一直为0，不会出现1的情况
            if((read_value.intensity >= set_value.threshold)
                && ((read_value.if_cnt <= IF_HIGH) && (read_value.if_cnt >= IF_LOW))
                &&(read_value.stereo > 0))
#else
            //tune_true 此标志位一直为0，不会出现1的情况
            if((read_value.intensity >= set_value.threshold)
                && ((read_value.if_cnt <= IF_HIGH) && (read_value.if_cnt >= IF_LOW)))
#endif                
            {
                tune_status = FOUND_STATION;
            }
            else
            {
                tune_status = INVALID_STATION;
            }                
        }            
    }
    else
    {
        tune_status = FOUND_NOSTAION;
    }
        */
    return tune_status;    
}

/******************************************************************************
** Description:
**     Set mute to fm
** Param:
**     mute      1 mute, 0 not mute 
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _fm_set_mute(int mute)
{
    int flag = 0;
     unsigned short write_byte;  
    if(mute == 1)
    {
        set_value.hmute = 1;
    }
    else
    {
        set_value.hmute = 0;        
    }


    write_byte = OperationRTC6222_read(0x02);  
     if (mute)
	 {      
	  write_byte &= ~0x4000; 
	 }   
	 else
	 {       
	     write_byte |= 0x4000;   
	 }    
	 OperationRTC6222_write(write_byte,0x02);








	
   // _assemble_write_buffer();
  //  _write_one_reg(AR_WRITE_REG_ADDR + 0x01, &Write_Buffer[1]);
    
 //   flag = _write_opration();
    
    return flag;
}

/******************************************************************************
** Description:
**     Set tune to fm
** Param:
**     mute      1 tune, 0 not tune 
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _fm_set_tune(int tune)
{
    int flag = 0;
    
    if(tune == 1)
    {
        set_value.tune_start = 0;
    }
    else
    {
        set_value.tune_start = 1;        
    }
    
 //   _assemble_write_buffer();
  //  _write_one_reg(AR_WRITE_REG_ADDR + 0x02, &Write_Buffer[2]);
    
    return flag;
}

/******************************************************************************
** Description:
**     Set band mode
** Param:
**     band      the band mode to be set 
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _fm_set_band_mode(int band)
{
    int flag = 0;

    if(band != set_value.band)
    {
        if(band == JAPAN)
        {
            set_value.band = 1;
            set_value.space = 100;
        }
        else if(band == EURO)
        {
            set_value.band = 0;
            set_value.space = 50;
        }
        else if(band == US)
        {
            set_value.band = 0;
            set_value.space = 100;
        }
        else
        {
            set_value.band = 0;
            set_value.space = 100;
        }
        
      //  _assemble_write_buffer();
   //     flag = _write_opration();
    }

    return (flag);
}

/******************************************************************************
** Description:
**     Set frequency 
** Param:
**     freq      the freqency to be set 
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
** Algorithm:
**     
*******************************************************************************/
static int _fm_set_freq(int freq)
{
    int flag=-1;
    int set_status=-1,test;

 /*   flag = _is_freq_valid(freq);
    if(flag < 0)
    {
        printk_err();
        return -1;
    }    */


    unsigned short write_byte;  
	unsigned char timeout = 0; 
	unsigned short Seek_Status = 0;
	#ifndef __RTCFM_STEP_50K__    
	freq *= 10;
	#endif    
	printf("==RTC6222_set_freq: %d\n",freq);    
	freq=freq/100;
      printf("___________RTC6222_set_freq: %d\n",freq);    
/*////////////////////////////////////////////////////////////////////////////////////
   	test=OperationRTC6222_read(0x00);

     printf("---------0000-----OperationRTC6222_read: %x\n",test);   

	 test=OperationRTC6222_read(0x02);

     printf("---------0002-----OperationRTC6222_read: %x\n",test);   
	 	 test=OperationRTC6222_read(0x04);

     printf("---------0004-----OperationRTC6222_read: %x\n",test);   
	 	 	 test=OperationRTC6222_read(0x05);

     printf("---------0005-----OperationRTC6222_read: %x\n",test);   
	 
 	 	 test=OperationRTC6222_read(0x06);

     printf("---------0006-----OperationRTC6222_read: %x\n",test);   
	  	 test=OperationRTC6222_read(0x07);

     printf("---------0007-----OperationRTC6222_read: %x\n",test);   
	 	  	 test=OperationRTC6222_read(0x08);

     printf("---------0008-----OperationRTC6222_read: %x\n",test);   
	 
	 	  	 test=OperationRTC6222_read(0x09);

     printf("---------0009-----OperationRTC6222_read: %x\n",test);   
	  	  	 test=OperationRTC6222_read(0x11);

     printf("---------0011-----OperationRTC6222_read: %x\n",test);   
	 	  	  	 test=OperationRTC6222_read(0x20);

     printf("---------0020-----OperationRTC6222_read: %x\n",test);   

/////////////////////////////////////////////////////////////////////////////////////////////////*/	  
	OperationRTC6222_write(0x8000|freq,0x03);    
   //     mdelay(10);
	test=OperationRTC6222_read(0x03);

	printf("---------0003-----OperationRTC6222_read: %x\n",test);    

	//New Algorithm part 1=on 0=off by Danny 20150724   
	do{        

	mdelay(8);            
	// 8 = 40ms(Default) 2 = 10ms(Speed limit) by Danny 20150724    
	timeout++;        
	
	//printf("++++++++++++timeout= 0x%02X\n ",timeout);   
	
	Seek_Status = ((0xFC00 & OperationRTC6222_read(0x0B)) >> 10);   
	
	//add by Danny 20150702     //   WATCHDOG_CLR();        
	}while ((Seek_Status == 0) && ((0x4000 & OperationRTC6222_read(0x0A))== 0) && (timeout < 200));   

	
	printf("++++++++++++Seek_Status_STD1= 0x%02X\n ,timeout=%d\n",Seek_Status,timeout);   
	
	//Seek_Status = ((0xFC00 & OperationRTC6222_read(0x0B)) >> 10);   
	//add by Danny 20150702    printf("Seek_Status1= 0x%04X ",Seek_Status);   
	write_byte = freq & (~0x8000);  
	OperationRTC6222_write(write_byte,0x03);   
	
	while(0x4000 & OperationRTC6222_read(0x0A));   
	Seek_Status = ((0xFC00 & OperationRTC6222_read(0x0B)) >> 10); 
	//add by Danny 20150702   
	printf("+++++++001++++++Seek_Status2= 0x%04X\n ",Seek_Status);  
	if(Seek_Status == 0) 
		{           //When Get Seek_Satus BK0_B[15:10] all 6b' = 0, Return 1. add by Danny 20150703      
		return 1; 
		}   
		else
		{     
		return 0;  
		}








	/*
    _ada_set_mute(1);
    
    //1.set hmute bit
//    _fm_set_tune(1);
//    mdelay(100);
    //2.clear tune bit
    set_value.tune_start = 0;
    _assemble_write_buffer();
    flag = _write_one_reg(AR_WRITE_REG_ADDR + 0x02, &Write_Buffer[2]);
    if( flag < 0 )
    {
        printk_err();
        _ada_set_mute(0);
        return -1;        
    }
    //3.clear seek bit
    //4.set band/space/chan bit   
    set_value.freq = freq;
    _assemble_write_buffer();
    flag = _write_opration();   
    if(flag < 0)
    {
        printk_err();
        _ada_set_mute(0);
        return -1;        
    }
    //5.enable tune bit
    set_value.tune_start = 1;
    _assemble_write_buffer();
    flag = _write_one_reg(AR_WRITE_REG_ADDR + 0x02, &Write_Buffer[2]);
    if(flag < 0)
    {
        printk_err();
        _ada_set_mute(0);
        return -1;        
    }
    
    //6.clear hmute bit
    _fm_set_tune(0);
    
    mdelay(30);
    flag = _read_opration();   
    if(flag < 0)
    {
        printk_err();
        _ada_set_mute(0);
        return -1;        
    }
    
    _disassemble_read_buffer();
    if( read_value.complete_flag == 1 )
    {
        flag = _is_freq_reach_band_limit(read_value.freq);
        if(flag == 1)
        {
            set_status = -1;
        }
        else
        {
            printk_dbg("【FM】set freq:%d, intensity:%d\n",read_value.freq,read_value.intensity);
#if  FM_CFG_STEREO_CHECK > 0
            if((read_value.intensity >= set_value.threshold)
                && ((read_value.if_cnt <= IF_HIGH) && (read_value.if_cnt >= IF_LOW))
                &&(read_value.stereo > 0))

#else
            if((read_value.intensity >= set_value.threshold)
                && ((read_value.if_cnt <= IF_HIGH) && (read_value.if_cnt >= IF_LOW)))
#endif                
            {
                set_status = 0;
            }
            else
            {
                set_status = -1;
            }              
        }            
    }
    else
    {
        set_status = -1;
    }
    
    _ada_set_mute(0);  */
    
 //   return set_status;
}

/******************************************************************************
** Description:
**     Set frequency, but do not check this frequency is one available station 
** Param:
**     freq      the freqency to be set 
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
** Algorithm:
**     
*******************************************************************************/

//////////////////////////////////////////////////////////////
 unsigned char RTC6222_set_freq(unsigned int  channel_freq)
{
   return _fm_set_freq(channel_freq);   
}	


//#pragma location="FM_SEG_RTC6222"
 unsigned char RTC6222_seek(unsigned int  channel_freq)
{
    unsigned char Status0 = 0;
   printf("+++++00000006766+++++channel_freq=%d\n",channel_freq);



	


    Status0 = RTC6222_set_freq(channel_freq);  // Check Seek_States is 0 ornot by Danny 20150702
    //mdelay(20);

    if(1 == Status0)
    {//Get one ture Channnel
        return 1;
    }
    else 
    {
        mdelay(2);  // 均衡搜台速度
        return 0;
    }



}










/////////////////////////////////////////////////////////////////








static int _fm_set_freq_no_check(int freq)
{
    int flag=-1;

    printk("【DM】set freq no check:%d\n",freq);
    flag = _is_freq_valid(freq);
  //  if(flag < 0)
   // {
       // printk_err();
        //return -1;
    //}    
	
 //   _ada_set_mute(1);

  //  _fm_set_freq(freq);
	
    //1.set hmute bit
//    _fm_set_tune(1);
//    mdelay(100);
    //2.clear tune bit
/*    set_value.tune_start = 0;
    _assemble_write_buffer();
    _write_one_reg(AR_WRITE_REG_ADDR + 0x02, &Write_Buffer[2]);
    //3.clear seek bit
    //4.set band/space/chan bit   
    set_value.freq = freq;
    _assemble_write_buffer();
 //   flag = _write_opration();   
    if(flag < 0)
    {
        printk_err();
        _ada_set_mute(0);
        return -1;        
    }
    //5.enable tune bit
    set_value.tune_start = 1;
    _assemble_write_buffer();
    _write_one_reg(AR_WRITE_REG_ADDR + 0x02, &Write_Buffer[2]);
    //6.clear hmute bit
    _fm_set_tune(0);

    _ada_set_mute(0); */
    
    return 0;
}

/******************************************************************************
** Description:
**     To check whether earphone inserted or not
** Param:
**     param       return the status to top level. 1 inserted, 0 not inseted
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _fm_is_earphone_inserted(int param)
{
    int state=0;
  
    
    //enum 类型在#if预处理中都为0. 这里只能用if
    if (FM_EARPHONE_GPIO_GROUP == GPIO_GROUP_NULL)
    {
        return -1;
    }

    gpio_enable_input( FM_EARPHONE_GPIO_GROUP, FM_EARPHONE_PIN_NUM );
    udelay(10000);
    state = gpio_get_state( FM_EARPHONE_GPIO_GROUP, FM_EARPHONE_PIN_NUM );
    if( FM_EARPHONE_GPIO_ACT_LEVEL == 1 )
    {
        *((int *)param) = ( state == 1 ) ? 0: 1;
    }
    else if ( FM_EARPHONE_GPIO_ACT_LEVEL == 0 )
    {
        *((int *)param) = ( state == 1 ) ? 1: 0;
    }
    else
    {
        printk_err("FM_EARPHONE_GPIO_ACT_LEVEL :%d",FM_EARPHONE_GPIO_ACT_LEVEL);
    }
    
    return 0;
}

/******************************************************************************
** Description:
**     Get current frequency
** Param:
**     radio_info_getfreq   return radio frequency to top level
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _fm_get_freq(struct radio_info_t * radio_info_getfreq)
{
    int flag=-1;
    
  /*  flag = _read_opration();
    if(flag == -1)
    {
        return -1;
    }
    
    _disassemble_read_buffer();      
	*/
    radio_info_getfreq->current_freq = read_value.freq;  
    return 0;       
}

/******************************************************************************
** Description:
**     Get current signal intensity
** Param:
**     pIntensity   return radio intensity to top level
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _fm_get_intensity(int* pIntensity)
{
    int flag=-1;
    
   /* flag = _read_opration();
    if(flag == -1)
    {
        return -1;
    }
    _disassemble_read_buffer();
    *((int *) pIntensity) = read_value.intensity/10;
    */
    return 0;
}

/******************************************************************************
** Description:
**     Get sterero mode: 1 stereo mode, 0 mono mode
** Param:
**     None
** Return:
**     0 sucess
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _fm_get_stereo_mode(int* pMode)
{
    int flag=-1;
    
 /*   flag = _read_opration();
    if(flag == -1)
    {
        printk_err();
        return -1;
    }
    _disassemble_read_buffer();
	*/
    *((int *) pMode) = read_value.stereo;
    
    return 0;
}


/******************************************************************************
** Description:
**     Set standby to fm
** Param:
**     stanb      1 standby, 0 not standby 
** Return:
**     0 success
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
int _enter_standby(int stanb)
{
    int flag=-1;
    unsigned int temp_write_data = 0;
    if(stanb==1)
    	{
            OperationRTC6222_write(0x16AA,0x00);   
			mdelay(10);
    	}
 else if  (stanb==0)
 	{
                 OperationRTC6222_write(0x96AA,0x00);  
		 mdelay(10);

 	}
	
 /*   set_value.hmute = 0;
    set_value.enable = 0;
    _assemble_write_buffer();
    flag = _is_transmit_ok();
  _write_one_reg(AR_WRITE_REG_ADDR + 0, &temp_write_data);
    if(flag < 0)
    {
        printk_err("");
        return -1;
    }    
    */
    return 0;
}


/******************************************************************************/
/*!                    
* \par  Description:
*     fm复位功能, 通过控制与其相连的引脚的高低状态, 完成复位功能
* \param[in]    void
* \return       
* \retval      
* \note:    一般模组不需要; 有些需要
*******************************************************************************/
static void _fm_reset( void )
{
    //enum 类型在#if预处理中都为0. 这里只能用if
    if (FM_RESET_GPIO_GROUP == GPIO_GROUP_NULL)
    {
        return;
    }
    gpio_enable_output( FM_RESET_GPIO_GROUP, FM_RESET_PIN_NUM );
    
    //低,延时; 高,延时; 完成复位
    gpio_set_state( FM_RESET_GPIO_GROUP, FM_RESET_PIN_NUM, 0 );
    mdelay(3);
    gpio_set_state( FM_RESET_GPIO_GROUP, FM_RESET_PIN_NUM, 1 );
    mdelay(5);
}


/******************************************************************************
** Description:
**     FM hardware initialization
** Param:
**     init_band_mode    band mode value:0 japan 1 EROUPE/USA
** Return:
**     0 sucess
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _init_hw(int init_band_mode)
{
    int flag=-1;
    int i = 0,test;
    int wait_time = 100;
    unsigned int temp_write_data = 0;
    
    memset(&set_value, 0, sizeof(set_value));
    memset(&read_value, 0, sizeof(read_value));
//    memset(Write_Buffer, 0, sizeof(Write_Buffer));
    memset(Read_Buffer, 0, sizeof(Read_Buffer));

    set_value.enable = 1;
 //   _assemble_write_buffer();


    //   printf("+++++++++++++++++++++++_init_hw");


     _write_one_reg(AR_WRITE_REG_ADDR_6222 + 0x00, &Write_Buffer[0]);    

     mdelay(50);
	  test=OperationRTC6222_read(0x00);
    _write_one_reg(AR_WRITE_REG_ADDR_6222 + 0x00, &Write_Buffer[1]);    
	
      mdelay(100);
	   test=OperationRTC6222_read(0x00);
    _write_one_reg(AR_WRITE_REG_ADDR_6222 + 0x02, &Write_Buffer[2]); 
	 test=OperationRTC6222_read(0x02);
    _write_one_reg(AR_WRITE_REG_ADDR_6222 + 0x04, &Write_Buffer[3]);  
	 test=OperationRTC6222_read(0x04);
    _write_one_reg(AR_WRITE_REG_ADDR_6222 + 0x05, &Write_Buffer[4]); 
	test=OperationRTC6222_read(0x05);

   //_write_one_reg(AR_WRITE_REG_ADDR_6222 + 0x07, &Write_Buffer[5]); 
	// test=OperationRTC6222_read(0x07);

    _write_one_reg(AR_WRITE_REG_ADDR_6222 +0x08, &Write_Buffer[7]); 
     test=OperationRTC6222_read(0x08);
    _write_one_reg(AR_WRITE_REG_ADDR_6222 + 0x09, &Write_Buffer[8]); 
	 test=OperationRTC6222_read(0x09);
	
    _write_one_reg(AR_WRITE_REG_ADDR_6222 + 0x11, &Write_Buffer[9]); 
       test=OperationRTC6222_read(0x11);
    _write_one_reg(AR_WRITE_REG_ADDR_6222 + 0x20, &Write_Buffer[10]);
	 test=OperationRTC6222_read(0x20);
		//	test=OperationRTC6222_read(0x20);
	
//	printf("---------0020-----OperationRTC6222_read: %x\n",test);	

                     test=OperationRTC6222_read(0x06);
		_write_one_reg(AR_WRITE_REG_ADDR_6222 +0x06, &Write_Buffer[6]); 
		mdelay(100);
                    	 test=OperationRTC6222_read(0x06);

/*

        	test=OperationRTC6222_read(0x00);

     printf("---------0000-----OperationRTC6222_read: %x\n",test);   

	        test=OperationRTC6222_read(0x02);

     printf("---------0002-----OperationRTC6222_read: %x\n",test);   
	 	 test=OperationRTC6222_read(0x04);

     printf("---------0004-----OperationRTC6222_read: %x\n",test);   
	 	  test=OperationRTC6222_read(0x05);

     printf("---------0005-----OperationRTC6222_read: %x\n",test);   
	 
 	 	 test=OperationRTC6222_read(0x06);

     printf("---------0006-----OperationRTC6222_read: %x\n",test);   
	  	 test=OperationRTC6222_read(0x07);

     printf("---------0007-----OperationRTC6222_read: %x\n",test);   
	 	 test=OperationRTC6222_read(0x08);

     printf("---------0008-----OperationRTC6222_read: %x\n",test);   
	 
	 	 test=OperationRTC6222_read(0x09);

     printf("---------0009-----OperationRTC6222_read: %x\n",test);   
	  	  test=OperationRTC6222_read(0x11);

     printf("---------0011-----OperationRTC6222_read: %x\n",test);   
	 	   test=OperationRTC6222_read(0x20);

     printf("---------~~~~~~~~-----OperationRTC6222_read: %x\n",test);   




*/






 //printf("++++++++++++++001+++++++++_init_hw");







/*
    temp_write_data = Write_Buffer[0] & 0xfffe;
    _write_one_reg(AR_WRITE_REG_ADDR + 0, &temp_write_data);
    for(i = 1; i < sizeof(Write_Buffer)/sizeof(Write_Buffer[0]); i++ )
    {
        _write_one_reg(AR_WRITE_REG_ADDR + i, &Write_Buffer[i]);
    }
    _write_one_reg(AR_WRITE_REG_ADDR + 0, &Write_Buffer[0]);

    _read_one_reg(AR_READ_REG_ADDR + 1, &Read_Buffer[1]);
    read_value.complete_flag = (Read_Buffer[1] >> READ_STC_BIT) & (READ_STC_MASK);

    while((read_value.complete_flag == 0) && (wait_time > 0) )
    {
        _read_one_reg(AR_READ_REG_ADDR + 1, &Read_Buffer[1]);
        read_value.complete_flag = (Read_Buffer[1] >> READ_STC_BIT) & (READ_STC_MASK);
        mdelay(5);
        wait_time--;
    }
    if( wait_time == 0)
    {
        printk_err("fm driver init timeout\n");
        return -1;
    }

*/

	//printf("++++++++++++++002+++++++++_init_hw");

  //  _disassemble_read_buffer();


    set_value.freq = 87000;
    set_value.band = 0;
    set_value.threshold = SEEK_THRESHOLD_LOW;
    set_value.space = 100;
	
   // _assemble_write_buffer();
 //   flag = _write_opration();
   /* if(flag < 0)
    {
        printk_err("");
        return -1;
    }    */
  
    return 0;
}

/******************************************************************************
** Description:
**     FM hardware de-initialization
** Param:
**     None
** Return:
**     0 sucess
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
static int _exit_hw(void)
{
    _ada_set_mute(1);//解退出fm有啪啪声bug添加
    _enter_standby(1); //enter standby

    //增加此操作防止speaker出声音,降低功耗
    if(FM_EARPHONE_GPIO_GROUP != GPIO_GROUP_NULL)
    {
        gpio_disable_input( FM_EARPHONE_GPIO_GROUP, FM_EARPHONE_PIN_NUM );
    }
    
    return 0;
}

#pragma __PRQA_IGNORE_END__


/******************************************************************************
** Description:
**     设置fm的时钟源
** Param:
**     stanb      1 on, 0 off 
** Return:
**     0 success
**     -1 failed
*******************************************************************************/
int fm_set_clk_source( int on )
{
#if FM_CFG_EXTERAL_CLK > 0
    unsigned int tmp = 0;

    //使能fm_clk引脚
    tmp = act_readl( MFP_CTL1 );
    tmp |= (0x01<<12);
    act_writel( tmp, MFP_CTL1 );
    
    //配置fm_clk功能
    tmp = act_readl( CMU_FMCLK );
    if ( 1 == on )
    {
        tmp &= ~0x06;
        tmp |= FM_CFG_EXTERAL_CLK_SOURCE;
        tmp |= 0x01;
    }
    else
    {
        tmp &= ~0x01;
    }
    
    act_writel( tmp, CMU_FMCLK );
    //TODO: wait clk stable, then operate module
    mdelay(1);
#endif
    return 0;
}

/******************************************************************************
** Description:
**     供fm_module_init/fm_resume, 没有open(fm)时调用. 需要自己open/close I2C
** Param:
**     stanb      1 standby, 0 not standby 
** Return:
**     0 success
**     -1 failed
*******************************************************************************/
int fm_enter_standby(int stanb)
{
    _enter_standby(stanb);  
    
    return 0;
}

/******************************************************************************
** Description:
**     FM ioctl, provide interface for fm_inner.c to access FM IC INFO
** Param:
**     cmd    command for IOCTL
**     param  command parameter
** Return:
**     0 sucess
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
int fm_ioctl_hw(unsigned int cmd, unsigned long param)
{
    int ret = 0;
    switch (cmd)
    {
    case RADIO_GET_STEREO_MODE:
		printf("++++++++++++++++RADIO_GET_STEREO_MODE\n");
        ret = _fm_get_stereo_mode((int *)param);
        break;
    
    case RADIO_GET_INFO:
		printf("++++++++++++++++RADIO_GET_INFO\n");
        ret = _fm_check_seek_status((struct radio_info_t *)param);
        break;
    
    case RADIO_GET_INTENSITY:
		printf("++++++++++++++++RADIO_GET_INTENSITY\n");
        ret = _fm_get_intensity((int *)param);
        break;
        
    case RADIO_GET_BAND_MODE:
        break;
    
    case RADIO_GET_FREQ:
		printf("++++++++++++++++RADIO_GET_FREQ\n");
        ret = _fm_get_freq((struct radio_info_t *)param);
        break;
    
    case RADIO_GET_ANTENNA:
		printf("++++++++++++++++RADIO_GET_ANTENNA\n");
        ret = _fm_is_earphone_inserted(param);
        break;
    
    case RADIO_SET_FREQ:

		printf("+++++++000005+++++++++RADIO_SET_FREQ\n");
        ret = _fm_set_freq((int)param);
    
       //   ret = RTC6222_seek((int)param);
		
        break;
		
    case RADIO_SET_FREQ_NO_CHECK:
		printf("++++++++++++++++RADIO_SET_FREQ_NO_CHECK\n");
        ret = _fm_set_freq_no_check((int) param);
        break;
        
    case RADIO_SET_THRESHOLD:
		
		printf("++++++++++++++++RADIO_SET_THRESHOLD\n");
        set_value.threshold = (int)(param);
        if(set_value.threshold >= 10 )
        {
            set_value.threshold = SEEK_THRESHOLD_HIGH;
        }
        else if(set_value.threshold >= 7)
        {
            set_value.threshold = SEEK_THRESHOLD_MID;
        }
        else 
        {
            set_value.threshold = SEEK_THRESHOLD_LOW;
        }
        break;
    
    case RADIO_SET_BAND_MODE:
		printf("++++++++++++++++RADIO_SET_BAND_MODE\n");
        ret = _fm_set_band_mode((int)param);
        break;
    
    case RADIO_SET_PLAY:
		
		printf("++++++++++++++++RADIO_SET_PLAY\n");
       // ret = _fm_set_mute(0);
		
        break;
    
    case RADIO_SET_STOP:
		
		printf("++++++++++++++++RADIO_SET_STOP\n");
        ret = _fm_set_mute(1);
		
        break;
    
    case RADIO_SET_ENTER_STANDBY:
		printf("++++++++++++++++RADIO_SET_ENTER_STANDBY\n");
        ret = _enter_standby((int)param);
        break;
    
    case RADIO_SET_AUTO_SEEK:
		printf("++++++++++++++++RADIO_SET_AUTO_SEEK\n");
        ret = _fm_seek_start((struct radio_seek_info_t *)param);
        break;
    
    case RADIO_SET_AUTO_SEEK_BREAK:
		printf("++++++++++++++++RADIO_SET_AUTO_SEEK_BREAK\n");
        ret = _fm_seek_break((struct radio_info_t *)param);
        break;
        
    case RADIO_SET_TUNE:
		printf("++++++++++++++++RADIO_SET_TUNE\n");
        ret = _fm_tune_start((struct radio_seek_info_t *)param);
        break;
        
    default:
        ret = -1;
        break;
    }
    return ret;
}

/******************************************************************************
** Description:
**     FM initialization
** Param:
**     init_band_mode    band mode value:0 japan 1 EROUPE/USA
** Return:
**     0 sucess
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
int fm_init_hw(int band_mode_init)
{
    //fm clk和btclk复用，此处需要重新设置
    fm_set_clk_source( 1 );
    _fm_reset();
    _init_hw(band_mode_init);
    return 0;
}

/******************************************************************************
** Description:
**     FM de-initialization
** Param:
**     None
** Return:
**     0 sucess
**     -1 failed
** Example code:
**     \code
**           list example code in here
**     \endcode
*******************************************************************************/
int fm_exit_hw(void)
{
    _exit_hw();
    fm_set_clk_source( 0 );    
    return 0;
}


