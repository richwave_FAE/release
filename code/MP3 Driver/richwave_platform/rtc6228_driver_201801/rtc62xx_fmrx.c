#include <c8051f340.h>
#include <stddef.h>
#include "datatype.h"
#include "port.h"
#include "rtc622x_bitfield.h"
#include "rtc62xx_fmrx.h"
#include "rtc622x_ate.h"
//-----------------------------------------------------------------------------
// Defines
//-----------------------------------------------------------------------------
#define USING_STC_INT ((rtc622x_shadow[0x4] & RTC62_STCIEN) && ((rtc622x_shadow[0x7] & RTC62_GPIO2) == RTC62_GPIO2_INT))
#define USING_RDS_INT ((rtc622x_shadow[0x4] & RTC62_RDSIEN) && ((rtc622x_shadow[0x7] & RTC62_GPIO2) == RTC62_GPIO2_INT))
#define USBand 0
#define JapanBand 2
//extern	bit	WaitSTCInterrupt;
bit	WaitSTCInterrupt;

//#define F2D17A
//Current version S2D20A
#define M2D22A
#if 0
#define POWERUP_TIME 110    // Powerup delay in milliseconds
#endif

#define USE_CRYSTAL 0       // 0 = external RCLK, 1 = crystal oscillator
#define POWERUP_TIME 110    // Powerup delay in milliseconds
#define CRYSTAL_TIME 500    // Crystal stabilization delay in milliseconds

//-----------------------------------------------------------------------------
// Global variables
//-----------------------------------------------------------------------------
extern  bit APP_EMB_SWITCH;
extern	bit	PoweredUp;
extern	bit	SeekTuneInProc;
extern	bit Renew_CH_Flag;
extern	u8	idata cmd[8];
extern	u8	idata rsp[15];
extern	u8	chipFunction;

// This variables are used by the status commands.  Make sure to call those
// commands (fmRsqStatus, fmTuneStatus, or fmRdsStatus) prior to access.
extern	u8  xdata	Status;
extern	u8  xdata	RsqInts;
extern	u8  xdata	STC;
extern	u8  xdata	SMUTE;
extern	u8  xdata	BLTF;
extern	u8  xdata	AFCRL;
extern	u8  xdata	Valid;
extern	u8  xdata	Pilot;
extern	u8  xdata	Blend;
extern	u16 xdata	Freq;
extern	u8  xdata	RSSI;
extern	u8  xdata	ASNR;
extern	u16 xdata	AntCap;
extern	u8  xdata	FreqOff;
		u8  xdata	RdsInts;
		u8  xdata	RdsSync;
		u8  xdata	GrpLost;
		u8  xdata	RdsFifoUsed;
		u16 xdata	BlockA;
		u16 xdata	BlockB;
		u16 xdata	BlockC;
		u16 xdata	BlockD;
		u8  xdata	BleA;
		u8  xdata	BleB;
		u8  xdata	BleC;
		u8  xdata	BleD;
		u8  xdata   SF_GOLBAL = 0;
extern  u8  xdata   Band_Flag;
extern  u8	xdata	Channel_count;
extern  u16 xdata	Current_Freq;
extern  u16 xdata	Current_FMFreq;
extern  u16 xdata	Current_AMFreq;
extern  u16 xdata	Current_SWFreq;

typedef enum {USA, EUROPE, JAPAN} country_enum; // Could be expanded
extern	bit			RDSCollect;
extern	void		Rds_VarsInit(void);
extern	void 		FM_CHANNEL_LCD(void);
extern 	void		Clean_LCD_RDS_Info(void);
//-----------------------------------------------------------------------------
// Function prototypes
//-----------------------------------------------------------------------------
extern	void 		wait_ms(u16 ms);
		void 		wait_us(u16 us);
//		void		rtc62xx_reset(void);
//		void		fmTuneFreq(u16 frequency);
//static	void	fmSeekStart(u8 seekUp, u8 wrap);
static	void		fmTuneStatus(u8 cancel, u8 intack);
//static	void	fmRsqStatus(u8 intack);
		u16			chanToFreq(u16 channel) large;
static	u16			freqToChan(u16 frequency) large;


void rtc622x_powerup(u8 on)
{
	EA = 0;
	if (on)
	{
	    rtc622x_singlereg_write(RegBank0x0, RegAddr0x0, 0x16AA);
	    wait_ms(100);
	
	    rtc622x_singlereg_write(RegBank0x0, RegAddr0x0, 0x96AA);
	    // After write 0x96AA to turn on regulator
	    // need extra 30ms for stabilizing
	    wait_ms(30*TIMESCALE);
	    rtc622x_singlereg_write(RegBank0x0, RegAddr0x0, 0x96AA);
	    // After write 0x96AA to turn on regulator
	    // need extra 30ms for stabilizing
	    wait_ms(30*TIMESCALE);
	
	#if 1
	    // If using internal RC to enable external crystal,
	    // need extra 600ms
	    rtc622x_singlereg_write(RegBank0x0, RegAddr0x7, 0xD200);
	    wait_ms(600*TIMESCALE);
	    wait_ms(600);
	#endif
	
	    rtc622x_singlereg_write(RegBank0xA, RegAddr0x0, 0x85A9);
	    rtc622x_singlereg_write(RegBank0x5, RegAddr0x3, 0x2E2E);
	    rtc622x_singlereg_write(RegBank0x4, RegAddr0x6, 0x30C4);
	    rtc622x_singlereg_write(RegBank0x8, RegAddr0xE, 0x9D00);
	
	    // Enable Chip
		rtc622x_singlereg_write(RegBank0x0, RegAddr0x6, 0x8000);
		wait_ms(200*TIMESCALE);					// timing to enable chip - 200ms
	}
	else
	{
    	// Disable Chip
		rtc622x_singlereg_write(RegBank0x0, RegAddr0x6, 0x0000);
    	// Shut down regulator
    	rtc622x_singlereg_write(RegBank0x0, RegAddr0x0, 0x16AA);
	}
    
	EA = 1;
}

#if 0
void rtc622x_fmrx_set_space(u8 space)
{
    u16 spacetemp;

	rtc622x_singlereg_read(RegBank0x1, RegAddr0x3, &spacetemp);
    spacetemp &= ~RTC622X_FM_SPACE;
    spacetemp |= (space << 8);
    rtc622x_singlereg_write(RegBank0x1, RegAddr0x3, spacetemp);
}

void rtc622x_set_band(u8 band)
{
    rtc622x_singlereg_write(RegBank0x1, RegAddr0x5, 0x222E);	// 87.50 MHz
    //rtc622x_singlereg_write(RegBank0x1, RegAddr0x5, 0x1DB0);	// 76 MHz
    wait_ms(50);
    rtc622x_singlereg_write(RegBank0x1, RegAddr0x4, 0x2A30);	// 108.00 MHz
    wait_ms(50);
}
#endif

void rtc622x_set_volume(u8 volume) 
{
	u16 voltemp=0;
	rtc622x_singlereg_read(RegBank0x0, RegAddr0x2, &voltemp);
		
	//rtc622x_shadow[0x2] |= RTC62_DMUTE;
	voltemp &= ~RTC622X_VOLUME;
	voltemp |= volume;
	rtc622x_singlereg_write(RegBank0x0, RegAddr0x2, voltemp);
}

u16 rtc622x_fmrx_tune(u16 frequency)
{
	//u16 xdata Bandsel;
	u16 xdata chantemp, statustemp, FrequencyBottom, FrequencyTop;
	u16 CH;

	Renew_CH_Flag = 1;
	rtc622x_singlereg_read(RegBank0x0, RegAddr0x3, &chantemp);
		
    FrequencyBottom = 8750;
    FrequencyTop = 10800;

	if (frequency > FrequencyTop)
		frequency = FrequencyBottom;

	if (frequency < FrequencyBottom)
		frequency = FrequencyTop;

	chantemp &= ~RTC622X_FREQUENCY;
	// Write channel number to register 0x03 then set tune bit
	chantemp |= (frequency & (~RTC622X_TUNE));
	rtc622x_singlereg_write(RegBank0x0, RegAddr0x3, chantemp);
	
	chantemp |= RTC622X_TUNE;
	rtc622x_singlereg_write(RegBank0x0, RegAddr0x3, chantemp);

	//rtc622x_reg_write(NON_VALID, (RegBank0x0<<4) | 0xA);
	// Wait for std bit to be set
	do
    {
		wait_ms(50);
		rtc622x_singlereg_read(RegBank0x0, RegAddr0xA, &statustemp);
	} while ((statustemp & RTC622X_STD) == 0);

	// Write address 0x03 to clear tune bit
	chantemp &= ~RTC622X_TUNE;
	rtc622x_singlereg_write(RegBank0x0, RegAddr0x3, chantemp);

	do
    {
		wait_ms(10);
		rtc622x_singlereg_read(RegBank0x0, RegAddr0xA, &statustemp);
	} while ((statustemp & RTC622X_STD) != 0);

	rtc622x_singlereg_read(RegBank0x1, RegAddr0xE, &CH);
	
	CH &= RTC622X_READCH;

#ifdef _LCD_DISPLAY_
	FM_CHANNEL_LCD();
#endif
	return CH;
}

// hardware seek
u16 rtc622x_fmrx_seek(u8 seekup, u8 seekmode) 
{
	//u8 seekfail; //seek_mode=0 Wrap, seek_mode=1 stop at limit bound
	u16	seektemp, statustemp;
	u16 CH;
//	EA = 0;
	// Before seeking, disable clk hopping
	//rtc622x_singlereg_write(RegBank0x4, RegAddr0x0, 0x9018);
	rtc622x_singlereg_read(RegBank0x0, RegAddr0x5, &seektemp);

	// Set or clear seekmode bit in address 0x05
	if (seekmode) {
		seektemp |= RTC622X_SKMODE;
	} else {
		seektemp &= ~RTC622X_SKMODE;
	}

	// Set or clear seekup bit in address 0x05
	if(seekup) {
		seektemp |= RTC622X_SEEKUP;
	} else {
		seektemp &= ~RTC622X_SEEKUP;
	}

	// Set seek bit in address 0x05
	seektemp &= ~RTC622X_SEEK;
	rtc622x_singlereg_write(RegBank0x0, RegAddr0x5, seektemp);

	seektemp |= RTC622X_SEEK;
	rtc622x_singlereg_write(RegBank0x0, RegAddr0x5, seektemp);

	// Wait for std bit to be set
	do {
		// be read here to get the current READCHAN value.
		wait_ms(50);
		rtc622x_singlereg_read(RegBank0x0, RegAddr0xA, &statustemp);
	} while ((statustemp & RTC622X_STD) == 0);

#if 0
	// Store the SF/BL bit as an indicator of the success of the seek
	seekfail = (statustemp & RTC62_SFBL) >> 13;
	if( seekfail == 1)
		SF_GOLBAL = 1;
	else 
		SF_GOLBAL = 0;
#endif

	// Clear seek bit in address 0x05
	seektemp &= ~RTC622X_SEEK;
	rtc622x_singlereg_write(RegBank0x0, RegAddr0x5, seektemp);

	do { 
		wait_ms(10);
		rtc622x_singlereg_read(RegBank0x0, RegAddr0xA, &statustemp);
	} while ((statustemp & RTC622X_STD) != 0);

	// Read Bank/address 0x01/0x0E to get READCHAN value (optional)
	rtc622x_singlereg_read(RegBank0x1, RegAddr0xE, &CH);
	CH &= RTC622X_READCH;
	// The tuner is now set to the newly found channel if one was available
	// as indicated by the seek-fail bit.

#ifdef _LCD_DISPLAY_
	FM_CHANNEL_LCD();   // for embedded used
#endif

	Current_FMFreq = CH;  // sync current freq. after seek
	// After seeking, enable clk hopping
	//rtc622x_singlereg_write(RegBank0x4, RegAddr0x0, 0x9008);	
//	EA = 1;
	return (CH); //return seek channel

}



