// This file contains the bit definitions as of RTC62XX firmware 16
#ifndef _BITFIELD_H_
#define _BITFIELD_H_

enum RegBankIndex
	{RegBank0x0, RegBank0x1, RegBank0x2, RegBank0x3, RegBank0x4, RegBank0x5, RegBank0x6, RegBank0x7,
	 RegBank0x8, RegBank0x9, RegBank0xA, RegBank0xB, RegBank0xC, RegBank0xD, RegBank0xE, RegBank0xF};

enum RegAddrIndex
	{RegAddr0x0, RegAddr0x1, RegAddr0x2, RegAddr0x3, RegAddr0x4, RegAddr0x5, RegAddr0x6, RegAddr0x7,
	 RegAddr0x8, RegAddr0x9, RegAddr0xA, RegAddr0xB, RegAddr0xC, RegAddr0xD, RegAddr0xE, RegAddr0xF};

#define DEVICEID                0x0
#define CHIPID                  0x1
#define POWERCFG                0x2
#define CHANNEL                 0x3
#define SYSCONFIG               0x4		//	SYSCONFIG1
#define SEEKCONFIG1             0x5		//	SYSCONFIG2
#define POWERCONFIG             0x6		//	SYSCONFIG3
#define PADCONFIG               0x7		//	TEST1
#define ASNR_DEF                0x8		//	TEST2
#define SEEKCONFIG2             0x9		//	BOOTCONFIG
#define STATUS                  0xA		//	STATUSRSSI
#define RSSI_DEF                0xB		//	READCHAN
#define RDSA                    0xC
#define RDSB                    0xD
#define RDSC                    0xE
#define RDSD                    0xF

// Register 00h
#define RTC622X_PN              0xF000
#define RTC622X_MFGID           0x0FFF

// Register 01h
#define RTC622X_REV             0xFC00
#define RTC622X_DEV             0x03C0
#define RTC622X_FIRMWARE        0x003F

// Register 02h
#define RTC622X_DSMUTE          0x8000
#define RTC622X_DMUTE           0x4000	// will mark
#define RTC622X_MONO            0x2000
#define RTC622X_DE              0x1000
#define RTC622X_VOL0			0x0400	 
#define RTC622X_BLNDADJ         0x0300

#define RTC622X_BLNDADJ_DN12DB  0x0300
#define RTC622X_BLNDADJ_DN6DB   0x0200
#define RTC622X_BLNDADJ_UP6DB   0x0100
#define RTC622X_BLNDADJ_DEF     0x0000

#define RTC622X_SMUTER          0x00C0
#define RTC622X_SMUTEA          0x0030
#define RTC622X_VOLUME          0x000F

// Register 03h
#define RTC622X_TUNE            0x8000
#define RTC622X_FREQUENCY       0x7FFF

// Register 04h
#define RTC622X_AGCD            0x2000

// Register 05h
#define RTC622X_SEEK            0x8000
#define RTC622X_SEEKUP          0x4000
#define RTC622X_SKMODE          0x2000
#define RTC622X_RSSI_LOW_TH		0x0F00
#define RTC622X_RSSI_MONO_TH	0x001F

// Register 06h
#define RTC622X_ENABLE          0x8000
#define RTC622X_BLNDOFS        	0x0F00
#define RTC622X_VOLEXT        	0x0008
#define RTC622X_CLK_TYPE      	0x0007	 

// Register 07h
#define RTC622X_XOSCEN          0x8000
#define RTC622X_SRST            0x0800

#define RTC622X_GPIO3           0x0030
#define RTC622X_GPIO3_HI		0x0030
#define RTC622X_GPIO3_LO        0x0020
#define RTC622X_GPIO3_ST        0x0010
#define RTC622X_GPIO3_HIZ       0x0000

// Register 08h

// Register 09h
#define RTC622X_IF_EST_VAL     	0x007F

// Register 0Ah
#define RTC622X_STD             0x4000
#define RTC622X_SFBL            0x2000
#define RTC622X_ST              0x0400

// Register 0Bh
#define RTC622X_RSSI            0x00FF

// Register 0Ch

// Register 0Dh

// Register 0Eh

// Register 0Fh

// Register 10h
#define RTC622X_OFSTH			0xFF00
#define RTC622X_QLTTH			0x00FF

// Register 11h

// Register 12h
#define RTC622X_VOL0_AUTO_FIX	0x0800
#define RTC622X_IF_EST_TH		0x001F

// Register 13h
// Unit 10KHz
#define RTC622X_FM_SPACE		0x1F00

// Register 14h
// Unit 10KHz
#define RTC622X_FM_FREQTOP		0x7FFF

// Register 15h
// Unit 10KHz
#define RTC622X_FM_FREQBOT		0x7FFF

// Register 1Eh
#define RTC622X_READCH			0x7FFF

#endif
