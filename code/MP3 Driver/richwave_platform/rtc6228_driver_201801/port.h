#ifndef _PORT_H_
#define _PORT_H_
// For RTC62XX
sbit SDIO        = P0^0;  // Routed to SDIO on the rtc62xx Part
sbit SCLK		 = P0^1;  // Routed to SCLK on the rtc62xx Part
sbit Test        = P0^2;  // Routed to SENB on the rtc62xx Part
sbit IR_Level 	 = P0^3;  // should confige to Open-Drain, Digital;
sbit SENB      	 = P0^4;  
sbit RSTB        = P0^5;  // Routed to RSTB on the rtc62xx Part
sbit GPIO2       = P0^6;  // GPIO2 on the rtc62xx
sbit GPIO3       = P0^7;  // GPIO3 on the rtc62xx

sbit RstTg1P     = P1^0;  // Routed to RstTg1P on the rtc62xx Part
//sbit NULL      = P1^1;  
sbit GP1         = P1^3;  // Not currently in use
//sbit GP2         = P1^4;  // Not currently in use
//sbit GP3         = P1^5;  // Not currently in use
//sbit NULL     = P1^6;  
//sbit NULL     = P1^7;  

//sbit GPIO1     	 = P2^0;  // GPIO1 on the rtc62xx
//sbit LED         = P2^1;  // GPIO3 on the rtc62xx
//sbit GPIO3       = P2^1;  // Routed to GPIO3 on the rtc62xx Part
//sbit SclkTg1P	    = P2^2;  // SclkTg1P on the rtc62xx
//sbit SdioTg1P     = P2^3;  // SdioTg1P on the rtc62xx
sbit Key1     	 = P2^4;
sbit Key2     	 = P2^5;
sbit Key3     	 = P2^6;
sbit Key4     	 = P2^7;

// SDIO
#define SDIO_HEX 0x01
#define SDIO_OUT P0MDOUT


// GPIO1
#define GPIO1_HEX 0x01
#define GPIO1_OUT P2MDOUT
	
// GPIO2
#define GPIO2_HEX 0x40
#define GPIO2_OUT P0MDOUT
#define GPIO2_PIN 0x06  // GPIO2 must be on port 0 to support interrupt capability.

#if 0	
// GPIO3
#define GPIO3_HEX 0x02
#define GPIO3_OUT P2MDOUT
#endif

#endif
