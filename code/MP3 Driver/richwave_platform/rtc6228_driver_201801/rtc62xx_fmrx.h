#ifndef _RTC622X_FMRX_H_
#define _RTC622X_FMRX_H_
#define _LCD_DISPLAY_

#define TIMESCALE   20
extern	bit			WaitSTCInterrupt;
extern	bit			PoweredUp;
extern	u16 xdata	rtc622x_shadow[16];  // Declared in rtc620x_low.c
extern  void    i2c_read_OneReg(u8 Bank_Addr, u16 *reg_array);
        void    rtc622x_singlereg_write(u8 Bank_Addr, u8 Reg_Addr, u16 Reg_Value);
        void    rtc622x_singlereg_read(u8 Bank_Addr, u8 Reg_Addr, u16 *Reg_Value);
        void    rtc622x_reg_write(u8 data_valid, u8 Bank_Addr) large;
        void    rtc622x_reg_read(u8 read_bytes) large;

	void    rtc622x_powerup(u8 on);
        void    rtc622x_set_band(u8 band);
        void    rtc622x_set_volume(u8 volume);
//        void    rtc622x_set_mute(u8 mute);

        u16     rtc622x_fmrx_tune(u16 frequency);
        u16     rtc622x_fmrx_seek(u8 seekup, u8 seekmode);

		void	rtc622x_fmrx_seek_all(u16 *);
//        u16     rtc622x_get_frequency(void);
//        u8      rtc622x_get_rssi(void);
//		u8		rtc622xFMRX_autoseek(void);
#endif
