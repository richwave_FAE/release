#include <c8051f340.h>
#include "datatype.h"
#include "port.h"
#include "if.h"

#define	READ	1
#define	WRITE	0

extern xdata u16 I2cAddrWriteV;
xdata u16 I2C_ADDRESS ;

extern void wait_ms(u16 ms);
extern void wait_us(u16 us);
//extern void wait_ns(u16 ns);

#if 0
extern void _nop_(void);

u8 io2w_read_byte(u8 remaining_bytes);

static void	die(void)
{
	_nop_(); //	put	breakpoint here	during debug.
}
#endif

static void	i2c_start(u8 i2c_address, u8 operation)
{
    i8 i;

    SCLK = 1;
    // Configure	SDIO as	push-pull, 
	// without pull up resistor should be configured to push pull mode    
    //SDIO_OUT |=	SDIO_HEX;
    
    wait_us(1);
    SDIO = 0;
    wait_us(1);
    SCLK = 0;

    i2c_address = (i2c_address & 0xFE) | (operation &	0x01);

    for	( i	= 7; i >= 0; i-- )
    {
        SCLK = 0;
        wait_us(1);
        SDIO = ((i2c_address >> i)	& 0x01);
        wait_us(1);
        SCLK = 1;
        wait_us(1);
        //SCLK = 0;	// test
    }

    SDIO_OUT &=	~(SDIO_HEX);   // Configure SDIO as open-drain
    SCLK = 0;
    SDIO = 1;
    wait_us(2);
    SCLK = 1;
    wait_us(1);
    //SCLK = 0;	// test
}

static void	i2c_stop(void)
{
    // Configure	SDIO as	push-pull, 
	// without pull up resistor should be configured to push pull mode
    //SDIO_OUT |=	SDIO_HEX;    
    SCLK = 0;
    wait_us(20);
    SDIO = 0;
    wait_us(1);
    SCLK = 1;
    wait_us(1);
    SDIO = 1;
}

static void	i2c_write_byte(u8 wrdata)
{
    i8 i;

    // Configure	SDIO as	push-pull,
    // without pull up resistor should be configured to push pull mode
    //SDIO_OUT |=	SDIO_HEX;
    for	( i	= 7; i >= 0; i-- )
    {
        SCLK = 0;
        wait_us(1);
        SDIO = ((wrdata	>> i) &	0x01);
        wait_us(2);
        SCLK = 1;
        wait_us(1);
    }

    SDIO_OUT &=	~(SDIO_HEX);    // Configure SDIO	as open-drain
    SCLK = 0;
    SDIO = 1;
    wait_us(2);
    SCLK = 1;
    wait_us(1);

}

void i2c_write(u8 Bank_Addr, u16 *reg_array, u8 data_valid)
{
    //I2C_ADDRESS = I2cAddrWriteV;
    I2C_ADDRESS = 0xC8;
    i2c_start(I2C_ADDRESS, WRITE);

    // writing byte of Bank_Addr
	i2c_write_byte((u8)(Bank_Addr));	 

	if(data_valid == 1)
	{
		i2c_write_byte((u8)(reg_array[0] >> 8));    // Write the high byte
	    i2c_write_byte((u8)(reg_array[0] & 0xff));  // Write the low byte
	}

    i2c_stop();
}

static u8 i2c_read_byte(u8	remaining_bytes)
{
    i8 i;
    u8 rddata =	0;

    SDIO_OUT &=	~(SDIO_HEX);
    for( i = 7;	i >= 0;	i--	)
    {
        SCLK = 0;
        SDIO = 1;
        wait_us(1);
        wait_us(2);
        SCLK = 1;
        wait_us(1);
        rddata = ((rddata << 1)	| SDIO);
    }

	SCLK = 0;
	// set the acknowledge
	SDIO = 0;                            

    // Configure SDIO as push-pull, 
    // without pull up resistor should be configured to push pull mode
    //SDIO_OUT |=	SDIO_HEX;
    if (remaining_bytes	== 0)
        SDIO = 1;
    else
        SDIO = 0;

    wait_us(2);
    SCLK = 1;
    wait_us(1);

    return rddata;
}

void i2c_read(u8 lastreg, u16 *reg_array)
{
    u8 reg = 0xA; 	
    //I2C_ADDRESS = I2cAddrWriteV;
    I2C_ADDRESS = 0xC8;
    i2c_start(I2C_ADDRESS, READ);

    do
    {
        u16	shadow_high	= i2c_read_byte(1);             // Read the high byte
        u8 shadow_low =	i2c_read_byte(lastreg - reg);   // Read the low byte

        reg_array[reg] = shadow_high * 256 + shadow_low;
        reg	= (reg + 1)	& 0xf;
    }
    while	(reg !=	((lastreg +	1) & 0xf));

    i2c_stop();
}

void i2c_read_OneReg(u8 Bank_Addr, u16 *reg_array)
{
	u16	shadow_high;
	u8 shadow_low;
    //I2C_ADDRESS = I2cAddrWriteV;
    I2C_ADDRESS = 0xC8;

    i2c_start(I2C_ADDRESS, WRITE);

    // writing byte of Bank_Addr
	i2c_write_byte((u8)(Bank_Addr));

    i2c_stop();

	wait_ms(10);

    i2c_start(I2C_ADDRESS, READ);

    shadow_high	= i2c_read_byte(1);		   // Read the high	byte
    shadow_low =  i2c_read_byte(0);	 	   // Read the low byte

    reg_array[0] = shadow_high * 256 + shadow_low;

    i2c_stop();
}
