#ifndef _IF_H_
#define _IF_H_

void i2c_write(u8 Bank_Addr, u16 *reg_array, u8 data_valid);
void i2c_read(u8 lastreg, u16 *reg_array);
void i2c_read_OneReg(u8 Bank_Addr, u16 *reg_array);

#endif
