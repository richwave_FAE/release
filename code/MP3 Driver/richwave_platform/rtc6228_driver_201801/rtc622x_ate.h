/*---------------------------------------------------------
	The	USB	Test HostvProgram for C8051340 of SiLab USB-MCU
	The	Program	for	PC's ATE Host use USB interface.
	Filename: RTC625X_ATE.h				V0.1 2013/12/16
	(C)2013	TT Chang					Last change: v0.1

-----------------------------------------------------------*/

#ifndef  _USB_API_H_
#define  _USB_API_H_
#include "datatype.h"

#if 0
// UINT type definition
#ifndef _UINT_DEF_
#define _UINT_DEF_
typedef unsigned int UINT;
#endif  /* _UINT_DEF_ */

// BYTE type definition
#ifndef _BYTE_DEF_
#define _BYTE_DEF_
typedef unsigned char BYTE;
#endif   /* _BYTE_DEF_ */
#endif

// Get_Interrupt_Source() return value bit masks
// Note: More than one bit can be set at the same time.
#define	USB_RESET		0x01  // USB Reset Interrupt has occurred
#define	TX_COMPLETE		0x02  // Transmit Complete Interrupt has occurred
#define	RX_COMPLETE		0x04  // Receive Complete Interrupt has occurred
#define	FIFO_PURGE		0x08  // Command received (and serviced) from the host
                              // to purge the USB buffers
#define	DEVICE_OPEN		0x10  // Device Instance Opened on host side
#define	DEVICE_CLOSE	0x20  // Device Instance Closed on host side
#define	DEV_CONFIGURED	0x40  // Device has entered configured state
#define	DEV_SUSPEND		0x80  // USB suspend signaling present on bus
#define LIST_UPPER_BOUND 60	  // The Seek_all maximum number of channel list 	
// Function prototypes
		void		USB_Clock_Start(void) large;
		void		USB_Init(u16,u16,u8*,u8*,u8*,u8,u8,u16) large;
		u16			Block_Write(u8*, u16) large;
		u8			Block_Read(u8*, u8) large;
		u8			Get_Interrupt_Source(void) large;
		void		USB_Int_Enable(void) large;
		void		USB_Int_Disable(void) large;
		void		USB_Disable(void) large;
		void		USB_Suspend(void) large;
		u16			USB_Get_Library_Version(void) large;

#endif  /* _USB_API	_H_ */

#define KEY_1_FLAG  0x1
#define KEY_2_FLAG  0x2
#define KEY_3_FLAG  0x4
#define KEY_4_FLAG  0x8

#define NON_VALID 	0
#define IS_VALID 	1

#define	NoUseV		0

enum TunerState
	{FMPowerOn, AMPowerOn, SWPowerOn, FMMonoPowerOn, StandBy = 0xFE, PowerOff = 0xFF };
