#include <c8051f340.h>
//#include <stddef.h>
#include "datatype.h"
#include "port.h"
#include "if.h"
#include "rtc622x_bitfield.h"

#define SPI    0
#define I2C    1

u16	xdata    rtc622x_shadow[16];
extern  void        wait_ms(u16 ms);
extern  void        wait_us(u16 us);

void rtc622x_reg_read(u8 lastreg)
{
    i2c_read(lastreg, rtc622x_shadow);
}

void rtc622x_singlereg_read(u8 Bank_Addr, u8 Reg_Addr, u16 *Reg_Value)
{
    i2c_read_OneReg((Bank_Addr<<4) | (Reg_Addr & 0x0F), Reg_Value);
}

void rtc622x_reg_write(u8 data_valid, u8 Bank_Addr)
{
    i2c_write(Bank_Addr, rtc622x_shadow, data_valid);
}

void rtc622x_singlereg_write(u8 Bank_Addr, u8 Reg_Addr, u16 Reg_Value)
{
    u16 temp[2];
    temp[0] = Reg_Value;

    i2c_write((Bank_Addr<<4) | (Reg_Addr & 0x0F), temp, 1);
    wait_ms(40*20);
}