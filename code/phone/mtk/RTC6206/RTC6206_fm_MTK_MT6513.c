/*
 * RTC6206 FM driver
 *
 * Copyright (C) 2011 Richwave. Inc
 * 
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *
 * Author				Date				Description
 * SWLin				2012.07.11			Created
 * 
 *
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/delay.h> // udelay()
#include <linux/device.h> // device_create()
#include <linux/platform_device.h>
#include <linux/i2c.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/version.h>      /* constant of kernel version */
#include <asm/uaccess.h> // get_user()
#include <linux/gpio.h>	 
#include <linux/fm.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/dma-mapping.h>

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif
#include <mach/mt6573_gpio.h>
#include <mach/mt6573_typedefs.h>
#include <cust_gpio_usage.h>

/*Read index*/
#define RD_INDEX_STATUS				0
#define RD_INDEX_WORK_CHN			2
#define RD_INDEX_DEVICE_ID			12
#define RD_INDEX_CHIP_ID			14
#define RD_INDEX_POWER_CONFIG		16
#define RD_INDEX_TUN_CHN_SET		18
#define RD_INDEX_CONFIG1			20
#define RD_INDEX_CONFIG2			22
#define RD_INDEX_CONFIG3			24
#define RD_INDEX_CONFIG4			26
#define RD_INDEX_MAX                28

/*Write index*/
#define WR_INDEX_POWER_CONFIG		0
#define WR_INDEX_TUN_CHN_SET		2
#define WR_INDEX_CONFIG1			4
#define WR_INDEX_CONFIG2			6
#define WR_INDEX_CONFIG3			8
#define WR_INDEX_CONFIG4			10
#define WR_INDEX_MAX				12

#define RD4WR_OFFSET				16

/*Device info*/
#define RTC6206_DEV_NAME			"RTC6206"
#define RTC6206_SLAVE_ADDR				(0x20)//0x10
#define FM_PROC_FILE 		"fm"

/*Gpio Pin*/
#define RTC6206_CLK_PIN				GPIO_FM_CLK_PIN
#define RTC6206_RESET_PIN			37

/*Power Status*/
enum {
	POWER_OFF,
	POWER_ON,
};

/*Seek Direction*/
#define SEEK_DOWN					0
#define SEEK_UP						1

/*----------------------------------------------------------------------------*/
#define I2C_BUS_NUM_STATIC_ALLOC
#define I2C_STATIC_BUS_NUM  (0)

#define DELAY(dur)		{ \
								volatile unsigned short i; \
								for (i = 0; i < dur; i++) {} \
						}

/*Debug function*/
#define pr_red_info(fmt, arg ...)	  printk(KERN_INFO "\033[31m" fmt "\033[0m\n", ##arg)
#define pr_green_info(fmt, arg ...)   printk(KERN_INFO "\033[32m" fmt "\033[0m\n", ##arg)
#define pr_pos_info()                 printk(KERN_INFO "%s [%d]\n", __FUNCTION__, __LINE__)


/***********************ioctl command****************************/
#define FM_IOCTL_BASE				'R'
#define FM_IOCTL_ENABLE				_IOW(FM_IOCTL_BASE, 0, int)
#define FM_IOCTL_GET_ENABLE			_IOW(FM_IOCTL_BASE, 1, int)
#define FM_IOCTL_SET_TUNE			_IOW(FM_IOCTL_BASE, 2, int)
#define FM_IOCTL_GET_FREQ			_IOW(FM_IOCTL_BASE, 3, int)
#define FM_IOCTL_SEARCH				_IOW(FM_IOCTL_BASE, 4, int[4])
#define FM_IOCTL_STOP_SEARCH		_IOW(FM_IOCTL_BASE, 5, int)
#define FM_IOCTL_SET_VOLUME			_IOW(FM_IOCTL_BASE, 7, int)
#define FM_IOCTL_GET_VOLUME			_IOW(FM_IOCTL_BASE, 8, int)

//customer need customize the I2C port
#define RTC6206_I2C_PORT     0

#define FMDEBUG
#ifdef FMDEBUG
#define FM_DEBUG(f, s...) \
    do { \
        printk("FM -> " f, ## s); \
    } while(0)
#else
#define FM_DEBUG(f, s...)
#endif

/******************************************************************************
 * GLOBAL DATA
 *****************************************************************************/
/* Addresses to scan */
static const struct i2c_device_id fm_i2c_id ={RTC6206_DEV_NAME, 0};
static unsigned short force[] = {RTC6206_I2C_PORT, RTC6206_SLAVE_ADDR, I2C_CLIENT_END, I2C_CLIENT_END};
static const unsigned short * const forces[] = {force, NULL};
static struct i2c_client_address_data addr_data = {.forces = forces};

static struct proc_dir_entry *g_fm_proc = NULL;

struct fm_rtc6206
{
	uint16_t status;
	uint16_t current_freq;
	uint16_t volume;
	uint16_t chip_id;
    uint16_t min_freq; // KHz
    uint16_t max_freq; // KHz
	dev_t dev_t;
	struct class *cls;
	struct device *dev;
	struct cdev cdev;
	struct i2c_client *i2c_client;
	struct mutex rtc6206_mutex;
};

static struct i2c_client *rtc6206_client = NULL;
static struct fm_rtc6206 *rtc6206_data = NULL;

static ssize_t rtc6206_freq_show(struct device *dev, struct device_attribute *attr, char *buf);
static ssize_t rtc6206_freq_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
static ssize_t rtc6206_hw_seek_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
static ssize_t rtc6206_volume_show(struct device *dev, struct device_attribute *attr, char *buf);
static ssize_t rtc6206_volume_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);
static ssize_t rtc6206_rssi_show(struct device *dev, struct device_attribute *attr, char *buf);

static int rtc6206_probe(struct i2c_client *client, const struct i2c_device_id *id);
static int rtc6206_remove(struct i2c_client *client);
static int rtc6206_detect(struct i2c_client *client, int kind, struct i2c_board_info *info);

static DRIVER_ATTR(freq, S_IRUSR | S_IWUSR, rtc6206_freq_show, rtc6206_freq_store);
static DRIVER_ATTR(seek, S_IRUSR | S_IWUSR, NULL, rtc6206_hw_seek_store);
static DRIVER_ATTR(volume, S_IRUSR | S_IWUSR, rtc6206_volume_show, rtc6206_volume_store);
static DRIVER_ATTR(rssi, S_IRUSR | S_IWUSR, rtc6206_rssi_show, NULL);

static struct driver_attribute *rtc6206_attr_list[] = {
    &driver_attr_freq,
    &driver_attr_seek,    
    &driver_attr_volume,        
    &driver_attr_rssi,
};

static struct i2c_driver rtc6206_driver = {
	.probe     = rtc6206_probe,
	.remove    = rtc6206_remove,
	.detect    = rtc6206_detect,
	#ifndef CONFIG_HAS_EARLYSUSPEND
	.suspend   = rtc6206_suspend,
	.resume    = rtc6206_resume,
	#endif
	.driver.name = RTC6206_DEV_NAME,
	.id_table = &fm_i2c_id,
	.address_data = &addr_data,	
};

static u8 *I2CDMABuf_va = NULL;
static u32 I2CDMABuf_pa = NULL;

static int rtc6206_read_registers(struct fm_rtc6206 *fm, char *buf, int len)
{
	int i = 0, err = 0;
	struct i2c_client *client = fm->i2c_client;

	if(len < 8)
	{
		client->addr = client->addr & I2C_MASK_FLAG;
		client->timing = 400;  
		return i2c_master_recv(client, buf, len);
	}
	else
	{
		client->addr = client->addr & I2C_MASK_FLAG | I2C_DMA_FLAG;
		client->timing = 400;  
		err = i2c_master_recv(client, I2CDMABuf_pa, len);
		
	    if(err < 0)
	    {
			return err;
		}

		for(i = 0; i < len; i++)
		{
			buf[i] = I2CDMABuf_va[i];
		}
	}
	return 0;
}

static u16 rtc6206_read_register(struct fm_rtc6206 *fm, int rd_index)
{
	struct i2c_client *client = fm->i2c_client;
	char reg_val[RD_INDEX_MAX];
	char val_l, val_h;
	int size = rd_index + 2;
	int ret;

	ret = rtc6206_read_registers(fm, reg_val, size);
	if (ret < 0)
	{
		goto RECV_ERR;
	}
	val_h = reg_val[rd_index];
	val_l = reg_val[rd_index + 1];

	return (val_h << 8) | val_l;

RECV_ERR:
	return ret;
}

static int rtc6206_write_registers(struct fm_rtc6206 *fm, const char *buf, int len)
{
	int i = 0;
	struct i2c_client *client = fm->i2c_client;
	
	for(i = 0 ; i < len; i++)
	{
		I2CDMABuf_va[i] = buf[i];
	}

	if(len < 8)
	{
		client->addr = client->addr & I2C_MASK_FLAG;
		client->timing = 400;  
		return i2c_master_send(client, buf, len);
	}
	else
	{
		client->addr = client->addr & I2C_MASK_FLAG | I2C_DMA_FLAG;
		client->timing = 400;  
		return i2c_master_send(client, I2CDMABuf_pa, len);
	}    
}

static int rtc6206_write_register(struct fm_rtc6206 *fm, int wr_index, u16 wr_val)
{
	struct i2c_client *client = fm->i2c_client;
	char buf[RD_INDEX_MAX];
	char *tmp;
	char val_h = (wr_val >> 8) & 0xff;
	char val_l = wr_val & 0xff;
	int size = wr_index + 2;
	int ret;

	mutex_lock(&fm->rtc6206_mutex);
	if (wr_index == WR_INDEX_POWER_CONFIG)
	{
		buf[0] = val_h;
		buf[1] = val_l;
		ret = rtc6206_write_registers(fm, buf, size);
		if (ret < 0)
		{
			goto SEND_ERR;
		}
	}
	else
	{
		ret = rtc6206_read_registers(client, buf, wr_index + RD4WR_OFFSET);
		if (ret < 0)
		{
			goto RECV_ERR;
		}
		buf[wr_index + RD4WR_OFFSET] = val_h;
		buf[wr_index + RD4WR_OFFSET + 1] = val_l;
		tmp = buf + RD4WR_OFFSET;
		ret = rtc6206_write_registers(client, tmp, size);
		if (ret < 0)
		{
			goto SEND_ERR;
		}
	}
	mutex_unlock(&fm->rtc6206_mutex);

	return 0;

RECV_ERR:
SEND_ERR:
	mutex_unlock(&fm->rtc6206_mutex);
	return ret;
}

static int rtc6206_create_attr(struct device_driver *driver) 
{
	int idx, err = 0;
	int num = (int)(sizeof(rtc6206_attr_list)/sizeof(rtc6206_attr_list[0]));
	if (driver == NULL)
	{
		return -EINVAL;
	}

	for(idx = 0; idx < num; idx++)
	{
		if((err = driver_create_file(driver, rtc6206_attr_list[idx])))
		{            
			FM_DEBUG("driver_create_file (%s) = %d\n", rtc6206_attr_list[idx]->attr.name, err);
			break;
		}
	}    
	return err;
}

static int rtc6206_delete_attr(struct device_driver *driver)
{
	int idx ,err = 0;
	int num = (int)(sizeof(rtc6206_attr_list)/sizeof(rtc6206_attr_list[0]));

	if (!driver)
	return -EINVAL;

	for (idx = 0; idx < num; idx++) 
	{
		driver_remove_file(driver, rtc6206_attr_list[idx]);
	}

	return err;
}

static int rtc6206_read_id(struct fm_rtc6206 *fm)
{
	u16 dev_id;

	dev_id = rtc6206_read_register(fm, RD_INDEX_DEVICE_ID);
	if (dev_id < 0)
	{
		FM_DEBUG("Read id error\n");
		return dev_id;
	}
	fm->chip_id = dev_id;
	FM_DEBUG("FM device id: 0x%x!!!\n", dev_id);

	return 0;
}

static void rtc6206_reset(void)
{
	mt_set_gpio_mode(RTC6206_RESET_PIN, GPIO_CTP_EN_PIN_M_GPIO);
    mt_set_gpio_dir(RTC6206_RESET_PIN, GPIO_DIR_OUT);
	//mt_set_gpio_out(RTC6206_RESET_PIN, GPIO_OUT_ONE);
	//udelay(10);
	mt_set_gpio_out(RTC6206_RESET_PIN, GPIO_OUT_ZERO);
	udelay(100);
    mt_set_gpio_out(RTC6206_RESET_PIN, GPIO_OUT_ONE);
	udelay(100);
}

static void rtc6206_set_rst_pin(int value)
{
	mt_set_gpio_mode(RTC6206_RESET_PIN, GPIO_CTP_EN_PIN_M_GPIO);
    mt_set_gpio_dir(RTC6206_RESET_PIN, GPIO_DIR_OUT);
	if(value == 1)
		 mt_set_gpio_out(RTC6206_RESET_PIN, GPIO_OUT_ONE);
	else if(value == 0)
		mt_set_gpio_out(RTC6206_RESET_PIN, GPIO_OUT_ZERO);
}

void rtc6206_set_clk(int on)
{
	if(on)
	{
		mt_set_gpio_mode(GPIO_FM_CLK_PIN, GPIO_FM_CLK_PIN_M_CLK);	
		mt_set_gpio_dir(GPIO_FM_CLK_PIN, GPIO_DIR_OUT);
		mt_set_clock_output(GPIO_FM_CLK_PIN_CLK, GPIO_FM_CLK_PIN_FREQ);
	}
	else
	{
		mt_set_gpio_mode(GPIO_FM_CLK_PIN, GPIO_MODE_00);
		mt_set_gpio_dir(GPIO_FM_CLK_PIN, GPIO_DIR_OUT);
		mt_set_gpio_out(GPIO_FM_CLK_PIN, 0);	
	}
}


static int rtc6206_power_up(struct fm_rtc6206 *fm)
{

	char ckhopping_info[] = {0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x20,0x00,
							 0x02,0xE1, 0x28,0x78, 0x32,0x31, 0x16,0x24, 0x05,0x90, 0x57,0x14, 0x10,0x08,
							 0xD0,0x09, 0x7F,0x80, 0x3C,0x01, 0xF0,0xCA, 0x01,0x00, 0x00,0x00, 0x01,0x40,
							 0x47,0x00, 0x00,0x00};

	char swbank_info[] = {0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x80,0x00,
						  0x01,0x01, 0xC8,0xC8, 0x05,0x1C, 0x05,0x1C, 0x05,0x1C, 0x05,0x1C, 0x05,0x1C,
						  0x00,0x83, 0x7F,0x8A, 0xC8,0x01, 0xC8,0xC8,
						  0x14,0x00, 0x40,0x00, 0x00,0x01, 0x32,0x01, 0x00, 0x00};
	char power_up_info[] = {0xc0,0x01, 0x00,0x00, 0x94,0x00, 0x0c,0x1a, 0x00,0x48};	//space 100, diable AGC, volume a
	int ret;

	rtc6206_reset();
	mdelay(10);

	rtc6206_set_clk(1);
	mdelay(10);

	ret = rtc6206_write_registers(fm, ckhopping_info, 46);
	if (ret < 0)
	{
		goto WRITE_CKHOP_INFO_FAILED;
	}

	ret = rtc6206_write_registers(fm, swbank_info, 46);
	if (ret < 0)
	{
		goto WRITE_SWBANK_INFO_FAILED;
	}

	ret = rtc6206_write_registers(fm, power_up_info, 10);
	if (ret < 0)
	{
		goto WRITE_POWERUP_INFO_FAILED;
	}

	fm->status = POWER_ON;

	return 0;

WRITE_POWERUP_INFO_FAILED:
WRITE_SWBANK_INFO_FAILED:
WRITE_CKHOP_INFO_FAILED:
	return ret;
}

static int rtc6206_power_down(struct fm_rtc6206 *fm)
{
	char power_down_info[] = {0x00,0x41};
	int ret;

	ret = rtc6206_write_registers(fm, power_down_info, 2);
	if (ret < 0)
	{
		goto WRITE_CKHOP_INFO_FAILED;
	}
	pr_green_info("RTC6206 Power down");

	rtc6206_set_rst_pin(0);

	fm->status = POWER_OFF;

	return 0;

WRITE_CKHOP_INFO_FAILED:

	return ret;
}

static int rtc6206_init_set(struct fm_rtc6206 *fm)
{
	u16 reg_val;
	u8 channel_num;

	mutex_init(&fm->rtc6206_mutex);

	reg_val = rtc6206_read_register(fm, RD_INDEX_WORK_CHN);
	if (reg_val < 0)
	{
		goto READ_CHANNELNUM_FAILED;
	}
	channel_num = reg_val & 0xff;
	fm->current_freq = channel_num + 875;

	reg_val = rtc6206_read_register(fm, RD_INDEX_CONFIG2);
	if (reg_val < 0)
	{
		goto READ_CONFIG2_FAILED;
	}
	fm->volume = reg_val & 0xf;

	reg_val = rtc6206_read_register(fm, RD_INDEX_POWER_CONFIG);
	if (reg_val < 0)
	{
		goto READ_POWERCONFIG_FAILED;
	}

	if (!(reg_val & (1 << 6)) && (reg_val & 1))
	{
		fm->status = POWER_ON;
	}
	else if (reg_val & (1 << 6))
	{
		fm->status = POWER_OFF;
	}

	return 0;

READ_POWERCONFIG_FAILED:
READ_CONFIG2_FAILED:
READ_CHANNELNUM_FAILED:
	return reg_val;
}

static int rtc6206_set_pwstatus(struct fm_rtc6206 *fm, int status)
{
	int ret;

	switch (status)
	{
	case POWER_ON:
		ret = rtc6206_power_up(fm);
		break;

	case POWER_OFF:
		ret = rtc6206_power_down(fm);
		break;

	default:
		ret = -EINVAL;
	}

	return ret;
}

static int rtc6206_get_pwstatus(struct fm_rtc6206 *fm, int *status)
{
	*status = fm->status;

	return 0;
}

static int rtc6206_set_volume(struct fm_rtc6206 *fm, int volume)
{
	int ret;
	u16 reg_val;

	if ((volume < 0) || (volume > 15))
	{
		return -EINVAL;
	}

	reg_val = rtc6206_read_register(fm, RD_INDEX_CONFIG2);
	if (reg_val < 0)
	{
		return reg_val;
	}

	volume &= 0xf;
	reg_val = (reg_val & 0xfff0) | volume;

	ret = rtc6206_write_register(fm, WR_INDEX_CONFIG2, reg_val);
	if (ret < 0)
	{
		return ret;
	}

	fm->volume = volume;

	return 0;
}

static int rtc6206_get_volume(struct fm_rtc6206 *fm, int *volume)
{
	*volume = fm->volume;

	return 0;
}

static int rtc6206_get_rssi(struct fm_rtc6206 *fm, u8 *rssi)
{
	int ret;
	char buf[2];

	ret = rtc6206_read_registers(fm, buf, 2);
	if (ret < 0)
	{
		return ret;
	}

	*rssi = buf[1] & 0xff;

	return 0;
}

static int rtc6206_tune_freq(struct fm_rtc6206 *fm, int channel_freq)
{
	char tune_start_info[4] = {0xc0,0x01, 0x80,0xca};
	char tune_stop_info[4] = {0xc0,0x01, 0x00,0xca};
	char reg_data[RD_INDEX_MAX];
	int channel_num;
	int loop_counter = 0;
	int ret;

	if ((channel_freq > 1080) || (channel_freq < 875))
	{
		return -EINVAL;
	}

	//cal & set the channel number
	channel_num = channel_freq - 875; // channel space is 100kHz
	tune_start_info[3] = channel_num & 0xff;
//	tune_start_info[2] = (tune_start_info[2] & 0xfc) | (channel_num >> 8); // Since 100kHz as space, use the 255 as Max channel_num is enough
	tune_stop_info[3] = tune_start_info[3];

	ret = rtc6206_write_registers(fm, tune_start_info, 4);
	if (ret < 0)
	{
		return ret;
	}

	//wait STD=1
	do {
		ret = rtc6206_read_registers(fm, reg_data, 2);
		if (ret < 0)
		{
			return ret;
		}
		loop_counter++;
	} while (((reg_data[0] & 0x40) == 0) && (loop_counter < 0xffff));

	if (loop_counter == 0xffff)
	{
		return -EINVAL;
	}

	fm->current_freq = channel_num + 875;
/*=========================================================================*/
	//disable tune bit
	loop_counter = 0;
	ret = rtc6206_write_registers(fm, tune_stop_info, 4);
	if (ret < 0)
	{
		return ret;
	}

	//wait STD=0
	do {
		ret = rtc6206_read_registers(fm, reg_data, 2);
		if (ret < 0)
		{
			return ret;
		}
		loop_counter++;
	} while (((reg_data[0] & 0x40) != 0) && (loop_counter < 0xff));

	if (loop_counter == 0xff)
	{
		return -EINVAL;
	}

	return 0;
}

static int rtc6206_get_freq(struct fm_rtc6206 *fm, int *channel_freq)
{
	*channel_freq = fm->current_freq;

	return 0;
}

static int rtc6206_hw_seek(struct fm_rtc6206 *fm, int *user_seekbuf)
{
	char seek_start_info[2];
	char seek_stop_info[2] = {0xc0,0x01};
	char reg_data[RD_INDEX_MAX];
	int loop_counter = 0;
	int *seek_buf = user_seekbuf;
	int ret = 0;

	if ((seek_buf[0] > 1080) || (seek_buf[0] < 875))
	{
		return -EINVAL;
	}

	switch (seek_buf[1])
	{
		case SEEK_DOWN:
			seek_start_info[0] = 0xc1;
			seek_start_info[1] = 0x01;
			break;

		case SEEK_UP:
			seek_start_info[0] = 0xc3;
			seek_start_info[1] = 0x01;
			break;

		default:
			ret = -EINVAL;
			return ret;
	}

	if (seek_buf[0] != fm->current_freq)
	{
		ret = rtc6206_tune_freq(fm, seek_buf[0]);
		if (ret < 0)
		{
			return ret;
		}
	}

	//set seek bit
	ret = rtc6206_write_registers(fm, seek_start_info, 2);
	if (ret < 0)
	{
		return ret;
	}

	//wait STD=1
	do {
		ret = rtc6206_read_registers(fm, reg_data, 4);
		if (ret < 0)
		{
			return ret;
		}
		loop_counter++;
	} while (((reg_data[0] & 0x40) == 0) && (loop_counter < 0xffff) && ((reg_data[0] & 0x20) == 0));

	if (loop_counter == 0xffff)
	{
		return -EFAULT;
	}

	if ((reg_data[0] & 0x20) != 0) // SF, seek failed
	{
		ret = rtc6206_write_registers(fm, seek_stop_info, 2);
		if(ret < 0)
		{
			return ret;
		}
		pr_red_info("No appropriate channel");

		return -EFAULT;
	}

	fm->current_freq = reg_data[3] + 875;
//================================================================================================
	loop_counter = 0;
	//clear seek bit
	ret = rtc6206_write_registers(fm, seek_stop_info, 2);
	if (ret < 0)
	{
		return ret;
	}

	//wait STD = 0
	do {
		ret = rtc6206_read_registers(fm, reg_data, 2);
		if(ret < 0)
		{
			return ret;
		}
		loop_counter++;
	} while (((reg_data[0] & 0x40) != 0) && (loop_counter < 0xff));

	if (loop_counter == 0xff)
	{
		return -EFAULT;
	}

	return 0;
}

static int rtc6206_stop_hw_seek(struct fm_rtc6206 *fm)
{
	return 0;
}

static ssize_t rtc6206_freq_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct fm_rtc6206 *fm = i2c_get_clientdata(rtc6206_client);
	int freq;
	int ret;

	ret = rtc6206_get_freq(fm, &freq);
	if (ret < 0)
	{
		pr_red_info("Get freq failed! ret = %d", ret);
	}
	pr_green_info("Get freq = %d", freq);

	return sprintf(buf, "%d\n", freq);
}

static ssize_t rtc6206_freq_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct fm_rtc6206 *fm = i2c_get_clientdata(rtc6206_client);
	int freq;
	int ret;

	freq = (int)simple_strtoul(buf, NULL, 10);
	pr_green_info("user_freq :%d", freq);
	ret = rtc6206_tune_freq(fm, freq);
	if (ret < 0)
	{
		pr_red_info("Tune failed! ret = %d", ret);
	}

	if (ret == 0)
	{
		pr_green_info("Tune ok!");
	}

	return count;
}

static ssize_t rtc6206_hw_seek_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct fm_rtc6206 *fm = i2c_get_clientdata(rtc6206_client);
	int seek_flag;
	int ret;
	int seekbuf[4] = {1000, SEEK_UP, 0xffff, 0};

	seek_flag = (unsigned short)simple_strtoul(buf, NULL, 10);
	pr_green_info("seek_flag : %d", seek_flag);

	if (seek_flag == 1)
	{
		ret = rtc6206_hw_seek(fm, seekbuf);
		if (ret < 0)
		{
			pr_red_info("Seek failed! ret = %d", ret);
		}

		if (ret == 0)
		{
			pr_green_info("Seek ok!");
		}
	}

	return count;
}

static ssize_t rtc6206_volume_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct fm_rtc6206 *fm = i2c_get_clientdata(rtc6206_client);
	int vol = 1;
	int ret;

	ret = rtc6206_get_volume(fm, &vol);
	if (ret < 0)
	{
		pr_red_info("Get volume failed! ret = %d", ret);
	}
	pr_green_info("Get volume = %d", vol);

	return sprintf(buf, "%d\n", vol);
}

static ssize_t rtc6206_volume_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct fm_rtc6206 *fm = i2c_get_clientdata(rtc6206_client);
	u8 vol;
	int ret;

	vol = (unsigned short)simple_strtoul(buf, NULL, 10);
	ret = rtc6206_set_volume(fm, vol);
	if (ret < 0)
	{
		pr_red_info("Set volume failed! ret = %d", ret);
	}

	if (ret == 0)
	{
		pr_green_info("Set volume ok! Volume = %d", vol);
	}

	return count;
}

static ssize_t rtc6206_rssi_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct fm_rtc6206 *fm = i2c_get_clientdata(rtc6206_client);
	u8 rssi = -1;
	int ret;

	ret = rtc6206_get_rssi(fm, &rssi);
	if (ret < 0)
	{
		pr_red_info("Get Rssi failed! ret = %d", ret);
		return sprintf(buf, "%d\n", rssi);
	}
	pr_green_info("Get rssi = %d", rssi);

	return sprintf(buf, "%d\n", rssi);
}

/**********************************Misc Device Info*********************************/
static int rtc6206_misc_open(struct inode *inode, struct file *file)
{
	struct fm_rtc6206 *fm = container_of(inode->i_cdev, struct fm_rtc6206, cdev);

	FM_DEBUG("%s(%d)\n",__FUNCTION__,__LINE__);
	
	/*if (!rtc6206_client)
	{
		return -ENODEV;
	}*/

	nonseekable_open(inode, file);
	//fm = i2c_get_clientdata(rtc6206_client);
	file->private_data = fm;
	rtc6206_power_up(fm);

	rtc6206_read_id(fm);
	
	return 0;
}

static int rtc6206_misc_close(struct inode *inode, struct file *file)
{
	struct fm_rtc6206 *fm = file->private_data;

	FM_DEBUG("%s(%d)\n",__FUNCTION__,__LINE__);
	rtc6206_power_down(fm);

	return 0;
}

static DECLARE_MUTEX(fm_ops_mutex);

static int rtc6206_ioctl_poweron(struct fm_rtc6206*fm, struct fm_tune_parm *parm)
{
    int ret;
    struct i2c_client *client = fm->i2c_client;

    if (fm->status)
    {
        parm->err = FM_BADSTATUS;
        return -EPERM;
    }
	ret = rtc6206_power_up(fm);
	if(ret != 0)
	{
        parm->err = FM_FAILED;
        return -EPERM;
	}
	parm->err = FM_SUCCESS;
	
    return 0;
}

static int rtc6206_ioctl_poweroff(struct fm_rtc6206*fm)
{
    struct i2c_client *client = fm->i2c_client;

    rtc6206_power_down(fm);

    return 0;
}

static int rtc6206_ioctl_tune(struct fm_rtc6206 *fm, struct fm_tune_parm *parm)
{
	char tune_start_info[4] = {0xc0,0x01, 0x80,0xca};
	char tune_stop_info[4] = {0xc0,0x01, 0x00,0xca};
	char reg_data[RD_INDEX_MAX];
	int channel_num;
	int loop_counter = 0;
	int ret,val;

    FM_DEBUG("%s\n", __func__);

    if (!fm->status)
    {
        parm->err = FM_BADSTATUS;
        return -EPERM;
    } 

    if (parm->space == FM_SPACE_100K)
        val = 0x2000;
    else if (parm->space == FM_SPACE_200K)
        val = 0x0000;
    else
    {
        parm->err = FM_EPARM;
        return -EPERM;
    }

    if (parm->band == FM_BAND_UE) {
        val |= 0x0000;
        fm->min_freq = 875;
        fm->max_freq = 1080;
    }
    else if (parm->band == FM_BAND_JAPAN) {
        val |= 0x1000;
        fm->min_freq = 760;
        fm->max_freq = 900;
    }
    else if (parm->band == FM_BAND_JAPAN) {
        val |= 0x1800;
        fm->min_freq = 760;
        fm->max_freq = 1080;
    }
    else
    {
        parm->err = FM_EPARM;
        return -EPERM;
    }        

    if (unlikely(parm->freq < fm->min_freq || parm->freq > fm->max_freq)) {
        parm->err = FM_EPARM;
        return -EPERM;
    }

	//cal & set the channel number
	channel_num = parm->freq - 875; // channel space is 100kHz
	tune_start_info[3] = channel_num & 0xff;
//	tune_start_info[2] = (tune_start_info[2] & 0xfc) | (channel_num >> 8); // Since 100kHz as space, use the 255 as Max channel_num is enough
	tune_stop_info[3] = tune_start_info[3];

	ret = rtc6206_write_registers(fm, tune_start_info, 4);
	if (ret < 0)
	{	
		parm->err = FM_SEEK_FAILED;
		return ret;
	}

	//wait STD=1
	do {
		ret = rtc6206_read_registers(fm, reg_data, 2);
		if (ret < 0)
		{
			parm->err = FM_SEEK_FAILED;
			return ret;
		}
		loop_counter++;
	} while (((reg_data[0] & 0x40) == 0) && (loop_counter < 0xffff));

	if (loop_counter == 0xffff)
	{
		parm->err = FM_SEEK_FAILED;
		return -EINVAL;
	}

	parm->err = FM_SUCCESS;
	fm->current_freq = channel_num + 875;
	parm->freq = fm->current_freq;
/*=========================================================================*/
	//disable tune bit
	loop_counter = 0;
	ret = rtc6206_write_registers(fm, tune_stop_info, 4);
	if (ret < 0)
	{
		return ret;
	}

	//wait STD=0
	do {
		ret = rtc6206_read_registers(fm, reg_data, 2);
		if (ret < 0)
		{
			return ret;
		}
		loop_counter++;
	} while (((reg_data[0] & 0x40) != 0) && (loop_counter < 0xff));

	if (loop_counter == 0xff)
	{
		return -EINVAL;
	}

	return 0;
}

static int rtc6206_ioctl_seek(struct fm_rtc6206 *fm, struct fm_seek_parm *parm)
{
    int ret = 0;
    uint16_t val = 0;
	char seek_start_info[2];
	char seek_stop_info[2] = {0xc0,0x01};
	char reg_data[RD_INDEX_MAX];
	int loop_counter = 0;

	if (!fm->status)
	{
		parm->err = FM_BADSTATUS;
		return -EPERM;
	}

	if (parm->space == FM_SPACE_100K)
		val |= 0x2000;
	else if (parm->space == FM_SPACE_200K)
		val |= 0x0000;
	else
	{
		parm->err = FM_EPARM;
		return -EPERM;
	}

	if (parm->band == FM_BAND_UE) {
		val |= 0x0000;
		fm->min_freq = 875;
		fm->max_freq = 1080;
	}
	else if (parm->band == FM_BAND_JAPAN) {
		val |= 0x1000;
		fm->min_freq = 760;
		fm->max_freq = 900;
	}
	else if (parm->band == FM_BAND_JAPANW) {
		val |= 0x1800;
		fm->min_freq = 760;
		fm->max_freq = 1080;
	}
	else
	{
		FM_DEBUG("band:%d out of range\n", parm->band);
		parm->err = FM_EPARM;
		return -EPERM;
	}

	if (parm->freq < fm->min_freq || parm->freq > fm->max_freq) {
		FM_DEBUG("freq:%d out of range\n", parm->freq);
		parm->err = FM_EPARM;
		return -EPERM;
	}

	if (parm->seekdir == FM_SEEK_UP)
		val |= 0x8000;

	if (parm->seekth > 0x7F) {
		FM_DEBUG("seekth:%d out of range\n", parm->seekth);
		parm->err = FM_EPARM;
		return -EPERM;
	}
	val |= parm->seekth;

	switch (parm->seekdir)
	{
		case FM_SEEK_DOWN:
			seek_start_info[0] = 0xc1;
			seek_start_info[1] = 0x01;
			break;

		case FM_SEEK_UP:
			seek_start_info[0] = 0xc3;
			seek_start_info[1] = 0x01;
			break;

		default:
			ret = -EINVAL;
			return ret;
	}

	if (parm->freq != fm->current_freq)
	{
		ret = rtc6206_tune_freq(fm, parm->freq);
		if (ret < 0)
		{
			return ret;
		}
	}

	//set seek bit
	ret = rtc6206_write_registers(fm, seek_start_info, 2);
	if (ret < 0)
	{
		return ret;
	}

	//wait STD=1
	do {
		ret = rtc6206_read_registers(fm, reg_data, 4);
		if (ret < 0)
		{
			return ret;
		}
		loop_counter++;
	} while (((reg_data[0] & 0x40) == 0) && (loop_counter < 0xffff) && ((reg_data[0] & 0x20) == 0));

	if (loop_counter == 0xffff)
	{
		return -EFAULT;
	}

	if ((reg_data[0] & 0x20) != 0) // SF, seek failed
	{
		ret = rtc6206_write_registers(fm, seek_stop_info, 2);
		if(ret < 0)
		{
			return ret;
		}
		pr_red_info("No appropriate channel");

		return -EFAULT;
	}

	fm->current_freq = reg_data[3] + 875;
	parm->freq = fm->current_freq;
	parm->err = FM_SUCCESS;
//================================================================================================
	loop_counter = 0;
	//clear seek bit
	ret = rtc6206_write_registers(fm, seek_stop_info, 2);
	if (ret < 0)
	{
		return ret;
	}

	//wait STD = 0
	do {
		ret = rtc6206_read_registers(fm, reg_data, 2);
		if(ret < 0)
		{
			return ret;
		}
		loop_counter++;
	} while (((reg_data[0] & 0x40) != 0) && (loop_counter < 0xff));

	if (loop_counter == 0xff)
	{
		return -EFAULT;
	}

	return 0;
}

static int rtc6206_misc_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long argc)
{
	struct fm_rtc6206 *fm = file->private_data;
	int freq;
	int volume;
	int status;
	int ret = 0;

	FM_DEBUG("%s(%x)\n",__FUNCTION__,cmd);
	switch (cmd)
	{
	    case FM_IOCTL_POWERUP:
	    {
	        struct fm_tune_parm parm;
	        FM_DEBUG("FM_IOCTL_POWERUP\n");

	        if (copy_from_user(&parm, (void*)argc, sizeof(struct fm_tune_parm)))
	            return -EFAULT;
	        if (down_interruptible(&fm_ops_mutex))
	            return -EFAULT;
	        ret = rtc6206_ioctl_poweron(fm, &parm);
			FM_DEBUG("IOCTL POWER ON %d\n",ret);
	        up(&fm_ops_mutex);
	        if (copy_to_user((void*)argc, &parm, sizeof(struct fm_tune_parm)))
	            return -EFAULT;
			//fm_low_power_wa(1);
	        break;
	    }		
		case FM_IOCTL_POWERDOWN:
		{
			FM_DEBUG("FM_IOCTL_POWERDOWN\n");
			if (down_interruptible(&fm_ops_mutex))
				return -EFAULT;
			ret = rtc6206_ioctl_poweroff(fm);
			FM_DEBUG("IOCTL POWER OFF %d\n",ret);
			up(&fm_ops_mutex);
			//fm_low_power_wa(0);
			break;
		}
		case FM_IOCTL_IS_FM_POWERED_UP:
		{
			FM_DEBUG("FM_IOCTL_IS_FM_POWERED_UP\n");
			uint32_t powerup;
			rtc6206_get_pwstatus(fm, &powerup);
			if (copy_to_user((void*)argc, &powerup, sizeof(uint32_t)))
	            return -EFAULT;
			break;
		}
		case FM_IOCTL_TUNE:
		{
			struct fm_tune_parm parm;
			FM_DEBUG("FM_IOCTL_TUNE\n");

			if (copy_from_user(&parm, (void*)argc, sizeof(struct fm_tune_parm)))
				return -EFAULT;

			if (down_interruptible(&fm_ops_mutex))
				return -EFAULT;
			ret = rtc6206_ioctl_tune(fm, &parm);
			up(&fm_ops_mutex);

			if (copy_to_user((void*)argc, &parm, sizeof(struct fm_tune_parm)))
				return -EFAULT;

			break;
		}
		case FM_IOCTL_SEEK:
		{
			struct fm_seek_parm parm;
			FM_DEBUG("FM_IOCTL_SEEK\n");

			if (copy_from_user(&parm, (void*)argc, sizeof(struct fm_seek_parm)))
				return -EFAULT;

			if (down_interruptible(&fm_ops_mutex))
				return -EFAULT;
			ret = rtc6206_ioctl_seek(fm, &parm);
			up(&fm_ops_mutex);

			if (copy_to_user((void*)argc, &parm, sizeof(struct fm_seek_parm)))
				return -EFAULT;

			break;
		}
		case FM_IOCTL_SETVOL:
		{
			uint32_t vol;
			FM_DEBUG("FM_IOCTL_SETVOL\n");

			if(copy_from_user(&vol, (void*)argc, sizeof(uint32_t))) {
				FM_DEBUG("copy_from_user failed\n");
				return -EFAULT;
			}
			if (down_interruptible(&fm_ops_mutex))
				return -EFAULT;
			ret = rtc6206_set_volume(fm, vol);
			up(&fm_ops_mutex);
			break;
		}
		case FM_IOCTL_GETVOL:
		{
			uint32_t vol;
			FM_DEBUG("FM_IOCTL_GETVOL\n");

			if (down_interruptible(&fm_ops_mutex))
				return -EFAULT;
			ret = rtc6206_get_volume(fm, &vol);
			up(&fm_ops_mutex);

			if (copy_to_user((void*)argc, &vol, sizeof(uint32_t)))
				return -EFAULT;

			break;
		}
	    case FM_IOCTL_GETCHIPID:
	    {
	        uint16_t chipid;            

	        if (down_interruptible(&fm_ops_mutex))
	            return -EFAULT;
	            
	        //chipid = 0x1000;//fm->chip_id;
	        chipid = fm->chip_id;
	        FM_DEBUG("FM_IOCTL_GETCHIPID:%04x\n", chipid);      

	        up(&fm_ops_mutex);
	        
	        if (copy_to_user((void*)argc, &chipid, sizeof(uint16_t)))
	            return -EFAULT;
	                        
	        break;
	    }	
		case FM_IOCTL_GETRSSI:
		{
			uint32_t rssi;
			FM_DEBUG("FM_IOCTL_GETRSSI\n");

			if (down_interruptible(&fm_ops_mutex))
				return -EFAULT;

			ret = rtc6206_get_rssi(fm, &rssi);
			up(&fm_ops_mutex);

			if (copy_to_user((void*)argc, &rssi, sizeof(uint32_t)))
				return -EFAULT;

			break;
		}
		case FM_IOCTL_RW_REG:
		{
			struct fm_ctl_parm parm_ctl;
			FM_DEBUG("FM_IOCTL_RW_REG\n");

			if (copy_from_user(&parm_ctl, (void*)argc, sizeof(struct fm_ctl_parm)))
				return -EFAULT;

			if (down_interruptible(&fm_ops_mutex))
				return -EFAULT;
			
			if(parm_ctl.rw_flag == 0) //write
			{
				//ret = rtc6206_write_register(fm, parm_ctl.addr, parm_ctl.val);
			}
			else
			{
				//ret = rtc6206_read_register(fm, parm_ctl.addr, &parm_ctl.val);
			}
			
			up(&fm_ops_mutex);
			if ((parm_ctl.rw_flag == 0x01) && (!ret)) // Read success.
			{ 
				if (copy_to_user((void*)argc, &parm_ctl, sizeof(struct fm_ctl_parm)))
					return -EFAULT;
			}
			break;
		}
	}

	return ret;
}

static const struct file_operations rtc6206_fops = {
	.owner   = THIS_MODULE,
	.open    = rtc6206_misc_open,
	.release = rtc6206_misc_close,
	.ioctl   = rtc6206_misc_ioctl,
};

static int rtc6206_setup_cdev(struct fm_rtc6206 *fm)
{
    int err;

    err = alloc_chrdev_region(&fm->dev_t, 0, 1, FM_NAME);
    if (err) {
        FM_DEBUG("alloc dev_t failed\n");
        return -1;
    }

    FM_DEBUG("alloc %s:%d:%d\n", FM_NAME,
                MAJOR(fm->dev_t), MINOR(fm->dev_t));

    cdev_init(&fm->cdev, &rtc6206_fops);

    fm->cdev.owner = THIS_MODULE;
    fm->cdev.ops = &rtc6206_fops;

    err = cdev_add(&fm->cdev, fm->dev_t, 1);
    if (err) {
        FM_DEBUG("alloc dev_t failed\n");
        return -1;
    }

    fm->cls = class_create(THIS_MODULE, FM_NAME);
    if (IS_ERR(fm->cls)) {
        err = PTR_ERR(fm->cls);
        FM_DEBUG("class_create err:%d\n", err);
        return err;            
    }    
    fm->dev = device_create(fm->cls, NULL, fm->dev_t, NULL, FM_NAME);

    return 0;
}

static int rtc6206_proc_read(char *page, char **start, off_t off, int count, int *eof, void *data)
{
	int cnt= 0;
	FM_DEBUG("Enter rtc6206_proc_read.\n");
	if(off != 0)
		return 0;
	struct fm_rtc6206 *fm  = rtc6206_data;
	if (fm != NULL && fm->status) {
		cnt = sprintf(page, "1\n");
	} else {
		cnt = sprintf(page, "0\n");
	}
	*eof = 1;
	FM_DEBUG("Leave rtc6206_proc_read. cnt = %d\n", cnt);
	return cnt;
}


static int rtc6206_init(struct i2c_client *client)
{
	int ret,err;
	int dev_id;
	struct fm_rtc6206 *fm;

	FM_DEBUG("%s\n",__FUNCTION__);
	
    if (!(fm = kzalloc(sizeof(struct fm_rtc6206), GFP_KERNEL)))
    {
        FM_DEBUG("-ENOMEM\n");
        err = -ENOMEM;
        goto ERR_EXIT;
    }

	fm->i2c_client = client;
	rtc6206_client = client;
	rtc6206_data = fm;
	i2c_set_clientdata(client, fm);

    if ((err = rtc6206_setup_cdev(fm)))
    {
        goto ERR_EXIT;
    }

	I2CDMABuf_va = (u8 *)dma_alloc_coherent(NULL, 2096, &I2CDMABuf_pa, GFP_KERNEL);
    if(!I2CDMABuf_va)
	{
		FM_DEBUG("Allocate DMA I2C Buffer failed!\n");
		//goto err;
	}
	FM_DEBUG("VA(%x),PA(%x)",I2CDMABuf_va,I2CDMABuf_pa);

	/*Must power down for consumption, IC BUG*/
	rtc6206_power_up(fm);
	msleep(300);

	err = rtc6206_read_id(fm);
	if (err < 0)
	{
		goto ERR_EXIT;
	}

	rtc6206_init_set(fm);
	FM_DEBUG("POWER = %d, FREQ = %d\n",fm->status,fm->current_freq);
	
	rtc6206_power_down(fm);

	/***********Add porc file system*************/
	g_fm_proc = create_proc_entry(FM_PROC_FILE, 0444, NULL);
	if (g_fm_proc == NULL) {
		FM_DEBUG("create_proc_entry failed\n");
		err = -ENOMEM;
		goto ERR_EXIT;
	} else {
		g_fm_proc->read_proc = rtc6206_proc_read;
		g_fm_proc->write_proc = NULL;
		//g_fm_proc->owner = THIS_MODULE;
		FM_DEBUG("create_proc_entry success\n");
	}
	/********************************************/
	if((err = rtc6206_create_attr(&rtc6206_driver.driver)))
	{
		FM_DEBUG("create attribute err = %d\n", err);
		//goto exit_create_attr_failed;
	}
        
    return 0;

ERR_EXIT:
    kfree(fm);

    return err;
}

static int rtc6206_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    int err;
    FM_DEBUG("%s\n",__FUNCTION__);    
    if ((err = rtc6206_init(client)))
    {
        FM_DEBUG("fm_init ERR:%d\n", err);
        goto ERR_EXIT;
    }   
    
    return 0;   
    
ERR_EXIT:
    return err;    
}

static int rtc6206_destroy(struct fm_rtc6206 *fm)
{
    int err = 0;

    FM_DEBUG("%s\n", __func__);

    device_destroy(fm->cls, fm->dev_t);
    class_destroy(fm->cls);

    cdev_del(&fm->cdev);
    unregister_chrdev_region(fm->dev_t, 1);

    rtc6206_power_down(fm);

	/***********************************/
	remove_proc_entry(FM_PROC_FILE, NULL);
	
	/**********************************/

    // FIXME: any other hardware configuration ?

    // free all memory
    kfree(fm);

    return err;
}

static int rtc6206_remove(struct i2c_client *client)
{
	int err;

	struct fm_rtc6206 *fm = i2c_get_clientdata(client);
	if(fm)
	{	 
		rtc6206_destroy(fm);
		fm = NULL;
	}

	rtc6206_set_clk(0);
	
	if((err = rtc6206_delete_attr(&rtc6206_driver.driver)))
	{
		FM_DEBUG("cm3623_delete_attr fail: %d\n", err);
	} 

	if(I2CDMABuf_va)
	{
		dma_free_coherent(NULL, 2096, I2CDMABuf_va, I2CDMABuf_pa);
		I2CDMABuf_va = NULL;
		I2CDMABuf_pa = NULL;
	}
	
	FM_DEBUG("%s\n",__FUNCTION__);
	i2c_set_clientdata(client, NULL);

	return 0;
}


static int rtc6206_detect(struct i2c_client *client, int kind, struct i2c_board_info *info)
{
    FM_DEBUG("%s\n",__FUNCTION__);
    strcpy(info->type, RTC6206_DEV_NAME);
    return 0;
}

static int rtc6206_suspend(struct i2c_client *client, pm_message_t mesg)
{
	return 0;
}

static void rtc6206_early_suspend(struct early_suspend *h)
{
}

static int rtc6206_resume(struct i2c_client *client)
{
	return 0;
}

static void rtc6206_later_resume(struct early_suspend *h)
{
}

#ifdef CONFIG_HAS_EARLYSUSPEND
static struct early_suspend rtc6206_stop = {
	.level   = EARLY_SUSPEND_LEVEL_STOP_DRAWING,
	.suspend = rtc6206_early_suspend,
	.resume  = rtc6206_later_resume,
};
#endif

// =================================================================

static int mt_fm_probe(struct platform_device *pdev)
{
	int ret;

	FM_DEBUG("%s\n",__FUNCTION__);
	
	ret = i2c_add_driver(&rtc6206_driver);
	if (ret < 0)
	{
	    pr_red_info("%s: add i2c slave device rtc6206's driver error %d\n", __FUNCTION__, ret);
		goto ERR_ADD_DRIVER;
	}

	return 0;

ERR_ADD_DRIVER:
	//kfree(rtc6206_client);
ERR_ADD_DEVICE:
	return ret;
}

static int mt_fm_remove(struct platform_device *pdev)
{
	FM_DEBUG("%s\n",__FUNCTION__);
	
	i2c_del_driver(&rtc6206_driver);
}

static struct platform_driver mt_fm_dev_drv =
{
    .probe   = mt_fm_probe,
    .remove  = mt_fm_remove,
    .driver = {
        .name   = FM_NAME,
        .owner  = THIS_MODULE,    
    }
};

/*
 *  mt_fm_init
 */
static int __init mt_fm_init(void)
{
	int err = 0;

	FM_DEBUG("%s\n",__FUNCTION__);
	err = platform_driver_register(&mt_fm_dev_drv);
    if (err)
    {
        FM_DEBUG("platform_driver_register failed\n");
    }
    
	return err;
}

/*
 *  mt_fm_exit
 */
static void __exit mt_fm_exit(void)
{
    FM_DEBUG("%s\n",__FUNCTION__);
    platform_driver_unregister(&mt_fm_dev_drv);
}

module_init(mt_fm_init);
module_exit(mt_fm_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("MediaTek FM Driver");
MODULE_AUTHOR("William Chung <William.Chung@MediaTek.com>");


