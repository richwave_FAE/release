/*
 * RTC6206 FM driver
 *
 * Copyright (C) 2011 Richwave. Inc
 * 
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *
 * Author				Date				Description
 * SWLin				2012.07.11			Created
 * 
 *
 */

#include <linux/module.h>
#include <linux/slab.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/miscdevice.h>
#include <mach/mfp.h>
#include <asm/uaccess.h>

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif

/*Read index*/
#define RD_INDEX_STATUS				0
#define RD_INDEX_WORK_CHN			2
#define RD_INDEX_DEVICE_ID			12
#define RD_INDEX_CHIP_ID			14
#define RD_INDEX_POWER_CONFIG		16
#define RD_INDEX_TUN_CHN_SET		18
#define RD_INDEX_CONFIG1			20
#define RD_INDEX_CONFIG2			22
#define RD_INDEX_CONFIG3			24
#define RD_INDEX_CONFIG4			26
#define RD_INDEX_MAX                28

/*Write index*/
#define WR_INDEX_POWER_CONFIG		0
#define WR_INDEX_TUN_CHN_SET		2
#define WR_INDEX_CONFIG1			4
#define WR_INDEX_CONFIG2			6
#define WR_INDEX_CONFIG3			8
#define WR_INDEX_CONFIG4			10
#define WR_INDEX_MAX				12

#define RD4WR_OFFSET				16

/*Device info*/
#define RTC6206_DEV_NAME			"RTC6206"
#define RTC6206_ADDR				(0x20>>1)//0x10

/*Gpio Pin*/
#define RTC6206_CLK_PIN				82
#define RTC6206_RESET_PIN			105
#define RTC6206_SCL_PIN				143
#define RTC6206_SDA_PIN				144

/*Power Status*/
enum {
	POWER_OFF,
	POWER_ON,
};

/*Seek Direction*/
#define SEEK_DOWN					0
#define SEEK_UP						1

/*----------------------------------------------------------------------------*/
#define I2C_BUS_NUM_STATIC_ALLOC
#define I2C_STATIC_BUS_NUM  (0)

#define DELAY(dur)		{ \
								volatile unsigned short i; \
								for (i = 0; i < dur; i++) {} \
						}

/*Debug function*/
#define pr_red_info(fmt, arg ...)	  printk(KERN_INFO "\033[31m" fmt "\033[0m\n", ##arg)
#define pr_green_info(fmt, arg ...)   printk(KERN_INFO "\033[32m" fmt "\033[0m\n", ##arg)
#define pr_pos_info()                 printk(KERN_INFO "%s [%d]\n", __FUNCTION__, __LINE__)

/*Misc Device Name*/
#define FM_MISC_NAME				"RTC6206_FM"

/***********************ioctl command****************************/
#define FM_IOCTL_BASE				'R'
#define FM_IOCTL_ENABLE				_IOW(FM_IOCTL_BASE, 0, int)
#define FM_IOCTL_GET_ENABLE			_IOW(FM_IOCTL_BASE, 1, int)
#define FM_IOCTL_SET_TUNE			_IOW(FM_IOCTL_BASE, 2, int)
#define FM_IOCTL_GET_FREQ			_IOW(FM_IOCTL_BASE, 3, int)
#define FM_IOCTL_SEARCH				_IOW(FM_IOCTL_BASE, 4, int[4])
#define FM_IOCTL_STOP_SEARCH		_IOW(FM_IOCTL_BASE, 5, int)
#define FM_IOCTL_SET_VOLUME			_IOW(FM_IOCTL_BASE, 7, int)
#define FM_IOCTL_GET_VOLUME			_IOW(FM_IOCTL_BASE, 8, int)

struct fm_rtc6206
{
	struct i2c_client *client;
	struct mutex rtc6206_mutex;
	int status;
	u16 current_freq;
	u8 volume;
};

static struct i2c_client *rtc6206_client = NULL;
static struct fm_rtc6206 *rtc6206_data = NULL;

static int rtc6206_read_registers(struct fm_rtc6206 *fm, char *buf, int size)
{
	struct i2c_client *client = fm->client;

	return i2c_master_recv(client, buf, size);
}

static u16 rtc6206_read_register(struct fm_rtc6206 *fm, int rd_index)
{
	struct i2c_client *client = fm->client;
	char reg_val[RD_INDEX_MAX];
	char val_l, val_h;
	int size = rd_index + 2;
	int ret;

	ret = i2c_master_recv(client, reg_val, size);
	if (ret < 0)
	{
		goto RECV_ERR;
	}
	val_h = reg_val[rd_index];
	val_l = reg_val[rd_index + 1];

	return (val_h << 8) | val_l;

RECV_ERR:
	return ret;
}

static int rtc6206_write_registers(struct fm_rtc6206 *fm, char *buf, int size)
{
	struct i2c_client *client = fm->client;

	return i2c_master_send(client, buf, size);
}

static int rtc6206_write_register(struct fm_rtc6206 *fm, int wr_index, u16 wr_val)
{
	struct i2c_client *client = fm->client;
	char buf[RD_INDEX_MAX];
	char *tmp;
	char val_h = (wr_val >> 8) & 0xff;
	char val_l = wr_val & 0xff;
	int size = wr_index + 2;
	int ret;

	mutex_lock(&fm->rtc6206_mutex);
	if (wr_index == WR_INDEX_POWER_CONFIG)
	{
		buf[0] = val_h;
		buf[1] = val_l;
		ret = i2c_master_send(client, buf, size);
		if (ret < 0)
		{
			goto SEND_ERR;
		}
	}
	else
	{
		ret = i2c_master_recv(client, buf, wr_index + RD4WR_OFFSET);
		if (ret < 0)
		{
			goto RECV_ERR;
		}
		buf[wr_index + RD4WR_OFFSET] = val_h;
		buf[wr_index + RD4WR_OFFSET + 1] = val_l;
		tmp = buf + RD4WR_OFFSET;
		ret = i2c_master_send(client, tmp, size);
		if (ret < 0)
		{
			goto SEND_ERR;
		}
	}
	mutex_unlock(&fm->rtc6206_mutex);

	return 0;

RECV_ERR:
SEND_ERR:
	mutex_unlock(&fm->rtc6206_mutex);
	return ret;
}

static int rtc6206_read_id(struct fm_rtc6206 *fm)
{
	u16 dev_id;

	dev_id = rtc6206_read_register(fm, RD_INDEX_DEVICE_ID);
	if (dev_id < 0)
	{
		pr_red_info("Read id error");
		return dev_id;
	}

	pr_green_info("FM device id: 0x%x!!!", dev_id);

	return 0;
}

static int rtc6206_rstpin_config(void)
{
	unsigned long mft;
	int ret;

	mft = MFP_CFG_X(RFCTL15, AF3, DS1, F_PULL_NONE, S_PULL_DOWN, IO_OE); // Reset
	sprd_mfp_config(&mft, 1);

	ret = gpio_request(RTC6206_RESET_PIN, "FM_RESET");
	if (ret < 0)
	{
		pr_red_info("FM_RESET PIN request failed!");
		goto RESET_PIN_REQUEST_FAILED;
	}

	gpio_direction_output(RTC6206_RESET_PIN, 1); // Reset pin set high

	return 0;

RESET_PIN_REQUEST_FAILED:
	return ret;
}

static void rtc6206_reset(void)
{
	gpio_set_value(RTC6206_RESET_PIN, 0); // Reset pin set low
	udelay(1);
	gpio_set_value(RTC6206_RESET_PIN, 1); // Reset pin set high
	udelay(1);
}

static int rtc6206_power_up(struct fm_rtc6206 *fm)
{

	char ckhopping_info[] = {0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x20,0x00,
							 0x02,0xE1, 0x28,0x78, 0x32,0x31, 0x16,0x24, 0x05,0x90, 0x57,0x14, 0x10,0x08,
							 0xD0,0x09, 0x7F,0x80, 0x3C,0x01, 0xF0,0xCA, 0x01,0x00, 0x00,0x00, 0x01,0x40,
							 0x47,0x00, 0x00,0x00};

	char swbank_info[] = {0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x80,0x00,
						  0x01,0x01, 0xC8,0xC8, 0x05,0x1C, 0x05,0x1C, 0x05,0x1C, 0x05,0x1C, 0x05,0x1C,
						  0x00,0x83, 0x7F,0x8A, 0xC8,0x01, 0xC8,0xC8,
						  0x14,0x00, 0x40,0x00, 0x00,0x01, 0x32,0x01, 0x00, 0x00};
	char power_up_info[] = {0xc0,0x01, 0x00,0x00, 0x94,0x00, 0x0c,0x1a, 0x00,0x48};	//space 100, diable AGC, volume a
	int ret;

	ret = rtc6206_write_registers(fm, ckhopping_info, 46);
	if (ret < 0)
	{
		goto WRITE_CKHOP_INFO_FAILED;
	}

	ret = rtc6206_write_registers(fm, swbank_info, 46);
	if (ret < 0)
	{
		goto WRITE_SWBANK_INFO_FAILED;
	}

	ret = rtc6206_write_registers(fm, power_up_info, 10);
	if (ret < 0)
	{
		goto WRITE_POWERUP_INFO_FAILED;
	}
	pr_green_info("RTC6206 Power up!");

	fm->status = POWER_ON;

	return 0;

WRITE_POWERUP_INFO_FAILED:
WRITE_SWBANK_INFO_FAILED:
WRITE_CKHOP_INFO_FAILED:
	return ret;
}

static int rtc6206_power_down(struct fm_rtc6206 *fm)
{
#if 0
	char ckhopping_info[] = {0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x20,0x00,
							0x02,0xE1, 0x28,0x78, 0x32,0x31, 0x16,0x24, 0x05,0x90, 0x57,0x14, 0x10,0x08,
							0xD0,0x09, 0x7F,0x80, 0x3C,0x01, 0xF0,0xCA, 0x01,0x00, 0x00,0x00, 0x01,0x40,
							0x47,0x00, 0x00,0x00};
#endif
	char power_down_info[] = {0x00,0x41};
	int ret;

#if 0
	ret = rtc6206_write_registers(fm, ckhopping_info, 46);
	if (ret < 0)
	{
		goto WRITE_CKHOP_INFO_FAILED;
	}
#endif

	ret = rtc6206_write_registers(fm, power_down_info, 2);
	if (ret < 0)
	{
		goto WRITE_POWERDOWN_INFO_FAILED;
	}
	pr_green_info("RTC6206 Power down");

	fm->status = POWER_OFF;

	return 0;

WRITE_CKHOP_INFO_FAILED:

	return ret;
}

static int rtc6206_init_set(struct fm_rtc6206 *fm)
{
	u16 reg_val;
	u8 channel_num;

	mutex_init(&fm->rtc6206_mutex);

	reg_val = rtc6206_read_register(fm, RD_INDEX_WORK_CHN);
	if (reg_val < 0)
	{
		goto READ_CHANNELNUM_FAILED;
	}
	channel_num = reg_val & 0xff;
	fm->current_freq = channel_num + 875;

	reg_val = rtc6206_read_register(fm, RD_INDEX_CONFIG2);
	if (reg_val < 0)
	{
		goto READ_CONFIG2_FAILED;
	}
	fm->volume = reg_val & 0xf;

	reg_val = rtc6206_read_register(fm, RD_INDEX_POWER_CONFIG);
	if (reg_val < 0)
	{
		goto READ_POWERCONFIG_FAILED;
	}

	if (!(reg_val & (1 << 6)) && (reg_val & 1))
	{
		fm->status = POWER_ON;
	}
	else if (reg_val & (1 << 6))
	{
		fm->status = POWER_OFF;
	}

	return 0;

READ_POWERCONFIG_FAILED:
READ_CONFIG2_FAILED:
READ_CHANNELNUM_FAILED:
	return reg_val;
}

static int rtc6206_set_pwstatus(struct fm_rtc6206 *fm, int status)
{
	int ret;

	switch (status)
	{
	case POWER_ON:
		ret = rtc6206_power_up(fm);
		break;

	case POWER_OFF:
		ret = rtc6206_power_down(fm);
		break;

	default:
		ret = -EINVAL;
	}

	return ret;
}

static int rtc6206_get_pwstatus(struct fm_rtc6206 *fm, int *status)
{
	*status = fm->status;

	return 0;
}

static int rtc6206_set_volume(struct fm_rtc6206 *fm, int volume)
{
	int ret;
	u16 reg_val;

	if ((volume < 0) || (volume > 15))
	{
		return -EINVAL;
	}

	reg_val = rtc6206_read_register(fm, RD_INDEX_CONFIG2);
	if (reg_val < 0)
	{
		return reg_val;
	}

	volume &= 0xf;
	reg_val = (reg_val & 0xfff0) | volume;

	ret = rtc6206_write_register(fm, WR_INDEX_CONFIG2, reg_val);
	if (ret < 0)
	{
		return ret;
	}

	fm->volume = volume;

	return 0;
}

static int rtc6206_get_volume(struct fm_rtc6206 *fm, int *volume)
{
	*volume = fm->volume;

	return 0;
}

static int rtc6206_get_rssi(struct fm_rtc6206 *fm, u8 *rssi)
{
	int ret;
	char buf[2];

	ret = rtc6206_read_registers(fm, buf, 2);
	if (ret < 0)
	{
		return ret;
	}

	*rssi = buf[1] & 0xff;

	return 0;
}

static int rtc6206_tune_freq(struct fm_rtc6206 *fm, int channel_freq)
{
	char tune_start_info[4] = {0xc0,0x01, 0x80,0xca};
	char tune_stop_info[4] = {0xc0,0x01, 0x00,0xca};
	char reg_data[RD_INDEX_MAX];
	int channel_num;
	int loop_counter = 0;
	int ret;

	if ((channel_freq > 1080) || (channel_freq < 875))
	{
		return -EINVAL;
	}

	//cal & set the channel number
	channel_num = channel_freq - 875; // channel space is 100kHz
	tune_start_info[3] = channel_num & 0xff;
//	tune_start_info[2] = (tune_start_info[2] & 0xfc) | (channel_num >> 8); // Since 100kHz as space, use the 255 as Max channel_num is enough
	tune_stop_info[3] = tune_start_info[3];

	ret = rtc6206_write_registers(fm, tune_start_info, 4);
	if (ret < 0)
	{
		return ret;
	}

	//wait STD=1
	do {
		ret = rtc6206_read_registers(fm, reg_data, 2);
		if (ret < 0)
		{
			return ret;
		}
		loop_counter++;
	} while (((reg_data[0] & 0x40) == 0) && (loop_counter < 0xffff));

	if (loop_counter == 0xffff)
	{
		return -EINVAL;
	}

	fm->current_freq = channel_num + 875;
/*=========================================================================*/
	//disable tune bit
	loop_counter = 0;
	ret = rtc6206_write_registers(fm, tune_stop_info, 4);
	if (ret < 0)
	{
		return ret;
	}

	//wait STD=0
	do {
		ret = rtc6206_read_registers(fm, reg_data, 2);
		if (ret < 0)
		{
			return ret;
		}
		loop_counter++;
	} while (((reg_data[0] & 0x40) != 0) && (loop_counter < 0xff));

	if (loop_counter == 0xff)
	{
		return -EINVAL;
	}

	return 0;
}

#if 1
static int rtc6206_get_freq(struct fm_rtc6206 *fm, int *channel_freq)
{
	*channel_freq = fm->current_freq;

	return 0;
}
#endif

#if 0
static int rtc6206_get_freq(struct fm_rtc6206 *fm, int *channel_freq)
{
	u16 reg_val;

	reg_val = rtc6206_read_register(fm, RD_INDEX_WORK_CHN);
	if (reg_val < 0)
	{
		return reg_val;
	}

	reg_val &= 0xff;
	*channel_freq = reg_val + 875;

	return 0;
}
#endif

static int rtc6206_hw_seek(struct fm_rtc6206 *fm, int *user_seekbuf)
{
	char seek_start_info[2];
	char seek_stop_info[2] = {0xc0,0x01};
	char reg_data[RD_INDEX_MAX];
	int loop_counter = 0;
	int *seek_buf = user_seekbuf;
	int ret = 0;

	if ((seek_buf[0] > 1080) || (seek_buf[0] < 875))
	{
		return -EINVAL;
	}

	switch (seek_buf[1])
	{
	case SEEK_DOWN:
		seek_start_info[0] = 0xc1;
		seek_start_info[1] = 0x01;
		break;

	case SEEK_UP:
		seek_start_info[0] = 0xc3;
		seek_start_info[1] = 0x01;
		break;

	default:
		ret = -EINVAL;
		return ret;
	}

	if (seek_buf[0] != fm->current_freq)
	{
		ret = rtc6206_tune_freq(fm, seek_buf[0]);
		if (ret < 0)
		{
			return ret;
		}
	}

	//set seek bit
	ret = rtc6206_write_registers(fm, seek_start_info, 2);
	if (ret < 0)
	{
		return ret;
	}

	//wait STD=1
	do {
		ret = rtc6206_read_registers(fm, reg_data, 4);
		if (ret < 0)
		{
			return ret;
		}
		loop_counter++;
	} while (((reg_data[0] & 0x40) == 0) && (loop_counter < 0xffff) && ((reg_data[0] & 0x20) == 0));

	if (loop_counter == 0xffff)
	{
		return -EFAULT;
	}

	if ((reg_data[0] & 0x20) != 0) // SF, seek failed
	{
		ret = rtc6206_write_registers(fm, seek_stop_info, 2);
		if(ret < 0)
		{
			return ret;
		}
		pr_red_info("No appropriate channel");

		return -EFAULT;
	}

	fm->current_freq = reg_data[3] + 875;
//================================================================================================
	loop_counter = 0;
	//clear seek bit
	ret = rtc6206_write_registers(fm, seek_stop_info, 2);
	if (ret < 0)
	{
		return ret;
	}

	//wait STD = 0
	do {
		ret = rtc6206_read_registers(fm, reg_data, 2);
		if(ret < 0)
		{
			return ret;
		}
		loop_counter++;
	} while (((reg_data[0] & 0x40) != 0) && (loop_counter < 0xff));

	if (loop_counter == 0xff)
	{
		return -EFAULT;
	}

	return 0;
}

static int rtc6206_stop_hw_seek(struct fm_rtc6206 *fm)
{
	return 0;
}

static ssize_t rtc6206_freq_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct fm_rtc6206 *fm = i2c_get_clientdata(rtc6206_client);
	int freq;
	int ret;

	ret = rtc6206_get_freq(fm, &freq);
	if (ret < 0)
	{
		pr_red_info("Get freq failed! ret = %d", ret);
	}
	pr_green_info("Get freq = %d", freq);

	return sprintf(buf, "%d\n", freq);
}

static ssize_t rtc6206_freq_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct fm_rtc6206 *fm = i2c_get_clientdata(rtc6206_client);
	int freq;
	int ret;

	freq = (int)simple_strtoul(buf, NULL, 10);
	pr_green_info("user_freq :%d", freq);
	ret = rtc6206_tune_freq(fm, freq);
	if (ret < 0)
	{
		pr_red_info("Tune failed! ret = %d", ret);
	}

	if (ret == 0)
	{
		pr_green_info("Tune ok!");
	}

	return count;
}
static DEVICE_ATTR(rtc6206_freq, S_IRUSR | S_IWUSR, rtc6206_freq_show, rtc6206_freq_store);

static ssize_t rtc6206_hw_seek_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct fm_rtc6206 *fm = i2c_get_clientdata(rtc6206_client);
	int seek_flag;
	int ret;
	int seekbuf[4] = {1000, SEEK_UP, 0xffff, 0};

	seek_flag = (unsigned short)simple_strtoul(buf, NULL, 10);
	pr_green_info("seek_flag : %d", seek_flag);

	if (seek_flag == 1)
	{
		ret = rtc6206_hw_seek(fm, seekbuf);
		if (ret < 0)
		{
			pr_red_info("Seek failed! ret = %d", ret);
		}

		if (ret == 0)
		{
			pr_green_info("Seek ok!");
		}
	}

	return count;
}
static DEVICE_ATTR(rtc6206_hw_seek, S_IRUSR | S_IWUSR, NULL, rtc6206_hw_seek_store);

static ssize_t rtc6206_volume_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct fm_rtc6206 *fm = i2c_get_clientdata(rtc6206_client);
	int vol = 1;
	int ret;

	ret = rtc6206_get_volume(fm, &vol);
	if (ret < 0)
	{
		pr_red_info("Get volume failed! ret = %d", ret);
	}
	pr_green_info("Get volume = %d", vol);

	return sprintf(buf, "%d\n", vol);
}

static ssize_t rtc6206_volume_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct fm_rtc6206 *fm = i2c_get_clientdata(rtc6206_client);
	u8 vol;
	int ret;

	vol = (unsigned short)simple_strtoul(buf, NULL, 10);
	ret = rtc6206_set_volume(fm, vol);
	if (ret < 0)
	{
		pr_red_info("Set volume failed! ret = %d", ret);
	}

	if (ret == 0)
	{
		pr_green_info("Set volume ok! Volume = %d", vol);
	}

	return count;
}
static DEVICE_ATTR(rtc6206_volume, S_IRUSR | S_IWUSR, rtc6206_volume_show, rtc6206_volume_store);

static ssize_t rtc6206_rssi_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct fm_rtc6206 *fm = i2c_get_clientdata(rtc6206_client);
	u8 rssi = -1;
	int ret;

	ret = rtc6206_get_rssi(fm, &rssi);
	if (ret < 0)
	{
		pr_red_info("Get Rssi failed! ret = %d", ret);
		return sprintf(buf, "%d\n", rssi);
	}
	pr_green_info("Get rssi = %d", rssi);

	return sprintf(buf, "%d\n", rssi);
}
static DEVICE_ATTR(rtc6206_rssi, S_IRUSR | S_IWUSR, rtc6206_rssi_show, NULL);

/**********************************Misc Device Info*********************************/
static int rtc6206_misc_open(struct inode *inode, struct file *file)
{
	struct fm_rtc6206 *fm;

	if (!rtc6206_client)
	{
		return -ENODEV;
	}

	nonseekable_open(inode, file);
	fm = i2c_get_clientdata(rtc6206_client);
	file->private_data = rtc6206_data;
	rtc6206_power_up(fm);
	pr_green_info("FM open already!");

	return 0;
}

static int rtc6206_misc_close(struct inode *inode, struct file *file)
{
	struct fm_rtc6206 *fm = file->private_data;

	rtc6206_power_down(fm);
	pr_green_info("FM close already!");

	return 0;
}

static int rtc6206_misc_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long argc)
{
	struct fm_rtc6206 *fm = file->private_data;
	int freq;
	int volume;
	int status;
	int ret = 0;

	switch (cmd)
	{
	case FM_IOCTL_ENABLE:
		status = *((int *)argc);
		ret = rtc6206_set_pwstatus(fm, status);
		break;

	case FM_IOCTL_GET_ENABLE:
		ret = rtc6206_get_pwstatus(fm, (int *)argc);
		break;

	case FM_IOCTL_SET_TUNE:
		freq = *(int *)argc;
		ret = rtc6206_tune_freq(fm, freq);
		break;

	case FM_IOCTL_GET_FREQ:
		ret = rtc6206_get_freq(fm, (int *)argc);
		break;

	case FM_IOCTL_SEARCH:
		ret = rtc6206_hw_seek(fm, (int *)argc);
		break;

	case FM_IOCTL_STOP_SEARCH:
		ret = rtc6206_stop_hw_seek(fm);
		break;

	case FM_IOCTL_SET_VOLUME:
		volume = *(int *)argc;
		ret = rtc6206_set_volume(fm, volume);
		break;

	case FM_IOCTL_GET_VOLUME:
		ret = rtc6206_get_volume(fm, (int *)argc);
		break;
	}

	return ret;
}

static const struct file_operations rtc6206_misc_fops = {
	.owner   = THIS_MODULE,
	.open    = rtc6206_misc_open,
	.release = rtc6206_misc_close,
	.ioctl   = rtc6206_misc_ioctl,
};

static struct miscdevice rtc6206_misc_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name  = FM_MISC_NAME,
	.fops  = &rtc6206_misc_fops,
};

static int rtc6206_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct fm_rtc6206 *fm;
	int ret;
	int dev_id;

	ret = rtc6206_rstpin_config();
	if (ret < 0)
	{
		goto RESET_CONFIG_FAILED;
	}
	rtc6206_reset();

	fm = kzalloc(sizeof(*fm), GFP_KERNEL);
	if (!fm)
	{
		ret = -ENOMEM;
		goto ALLOC_FAILED;
	}
	fm->client = client;
	rtc6206_data = fm;
	i2c_set_clientdata(client, fm);

	ret = misc_register(&rtc6206_misc_device);
	if (ret < 0)
	{
		goto MISC_DEVICE_REGISTE_FAILED;
	}

	dev_id = rtc6206_read_id(fm);
	if (dev_id < 0)
	{
		goto READ_ID_FAILED;
	}

	/*Must power down for consumption, IC BUG*/
	rtc6206_power_up(fm);
	msleep(300);
	rtc6206_power_down(fm);

	rtc6206_init_set(fm);

	return 0;

READ_ID_FAILED:
	misc_deregister(&rtc6206_misc_device);
MISC_DEVICE_REGISTE_FAILED:
	i2c_set_clientdata(client, NULL);
	kfree(fm);
ALLOC_FAILED:
	gpio_free(RTC6206_RESET_PIN);
RESET_CONFIG_FAILED:
	return ret;
}

static int rtc6206_remove(struct i2c_client *client)
{
	struct fm_rtc6206 *fm = i2c_get_clientdata(client);

	misc_deregister(&rtc6206_misc_device);
	i2c_set_clientdata(client, NULL);
	kfree(fm);
	gpio_free(RTC6206_RESET_PIN);

	return 0;
}

static int rtc6206_suspend(struct i2c_client *client, pm_message_t mesg)
{
	return 0;
}

static void rtc6206_early_suspend(struct early_suspend *h)
{
}

static int rtc6206_resume(struct i2c_client *client)
{
	return 0;
}

static void rtc6206_later_resume(struct early_suspend *h)
{
}

static struct i2c_device_id rtc6206_idtable[] = {
	[0] = {
		.name = RTC6206_DEV_NAME,
		.driver_data = 0,
	},
	[1] = {},
};

static struct i2c_driver rtc6206_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name  = RTC6206_DEV_NAME,
	},
	.id_table  = rtc6206_idtable,
	.probe     = rtc6206_probe,
	.remove    = rtc6206_remove,
#ifndef CONFIG_HAS_EARLYSUSPEND
	.suspend   = rtc6206_suspend,
	.resume    = rtc6206_resume,
#endif
};

#ifdef CONFIG_HAS_EARLYSUSPEND
static struct early_suspend rtc6206_stop = {
	.level   = EARLY_SUSPEND_LEVEL_STOP_DRAWING,
	.suspend = rtc6206_early_suspend,
	.resume  = rtc6206_later_resume,
};
#endif

// =================================================================
#ifdef I2C_BUS_NUM_STATIC_ALLOC
static struct i2c_board_info  rtc6206_i2c_boardinfo = {
	.type = RTC6206_DEV_NAME,
	.addr = RTC6206_ADDR,
};

static int i2c_static_add_device(struct i2c_board_info *info)
{
	struct i2c_adapter *adapter;
	struct i2c_client *client;
	int err;

	adapter = i2c_get_adapter(I2C_STATIC_BUS_NUM);
	if (!adapter)
	{
		pr_err("%s: can't get i2c adapter\n", __FUNCTION__);
		err = -ENODEV;
		goto I2C_ERR;
	}

	client = i2c_new_device(adapter, info);
	if (!client)
	{
		pr_err("%s:  can't add i2c device at 0x%x\n",
			__FUNCTION__, (unsigned int)info->addr);
		err = -ENODEV;
		goto I2C_ERR;
	}

	i2c_put_adapter(adapter);
	rtc6206_client = client;

	return 0;

I2C_ERR:
	return err;
}
#endif

static __init int rtc6206_driver_init(void)
{
	int ret;

	ret = i2c_static_add_device(&rtc6206_i2c_boardinfo);
	if (ret < 0)
	{
	    pr_red_info("%s: add i2c slave device rtc6206 error %d\n", __FUNCTION__, ret);
		goto ERR_ADD_DEVICE;
	}

	ret = i2c_add_driver(&rtc6206_driver);
	if (ret < 0)
	{
	    pr_red_info("%s: add i2c slave device rtc6206's driver error %d\n", __FUNCTION__, ret);
		goto ERR_ADD_DRIVER;
	}

	return 0;

ERR_ADD_DRIVER:
	kfree(rtc6206_client);
ERR_ADD_DEVICE:
	return ret;
}

static __exit void rtc6206_driver_exit(void)
{
	i2c_del_driver(&rtc6206_driver);

#ifdef I2C_BUS_NUM_STATIC_ALLOC
	if (rtc6206_client)
	{
		i2c_unregister_device(rtc6206_client);
	}
#endif
}

module_init(rtc6206_driver_init);
module_exit(rtc6206_driver_exit);

MODULE_AUTHOR("YRLee <yrlee@richwave.com.tw>");
MODULE_DESCRIPTION("RTC6206 FM Driver");
MODULE_LICENSE("GPL");
